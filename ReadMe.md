## LSUHSC-PIW Data Migration Project

### Scope

[The Partners in Wellness](http://www.lsuhscshreveport.edu/departments/centers-of-excellence/feist-weiller-cancer-center-clone/patients/patient-guide/feist-weiller-cancer-center-hematology-oncology-clone "LSUHSC PIW Page") cancer screening center performs numerous screenings, frequently at little or no charge to patients, utilizing their office and mobile treatment vans. To facilitate follow-up and research, they have collected a large amount of data over the years and have collected it in different formats. For about 15 years they used a MS Access database. Due to data synchronization problems and other issues, they decided several years ago to move to [REDCap](https://redcap.lsuhscs.edu/index.php?action=myprojects "LSUHSC REDCap") but have not moved the data from the legacy database until now. This project is the attempt at getting the data moved over. Only dummy data is used in this project. _Do not put live data in a public repository, or even dummy data._ If you would like a copy of the dummy data, contact myself or Dr McLarty.
Access the REDCap database at this [link](https://redcap.lsuhscs.edu "Contact Dr McLarty for access")
### Data Migration Steps

The majority of this project has not yet been automated. Feel free to add some. Currently there are manual steps that I have been using:

1. Find a computer with MS Windows, preferably a later version that will work with MS SQL Server
  * I checked out an AWS cloud instance to avoid licensing issues
2. Install SQL Server Migration Assistant and SQL Server Management Studio
  * You'll need a reasonable version of SQL Server, not Express, but Developer or better
  * The 64 bit version of all installed dependencies should be fine, but the dummy data is 32 bit
3. Run the Migration Assistant and import the tables into a database called 695Test
4. Create .csv files from the data to match the data import templates in the REDCap site
5. Upload the files to the database

Simple, right? Well, the queries aren't finished yet.

Also...there's a dummy data sql file. If you're using real data, this could be really bad. It's going to set 
all of your data to random values. It's designed to populate a scrubbed database with random data that 
passes the input validation in REDCap. 
