/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [PAPID]
      ,[PtID]
      ,[VisitID]
      --,[VisitDate]
      --,[LASTUPDT]
      --,[ResultDate]
      ,[Adequacy]
      ,[PapType]
      ,[RSPAP]
      ,[ResultsPap]
      ,[OTHPAPRS]
      ,[PAPRCMD]
      ,[RECPAP]
      ,[PAPFUDATE]
      ,[OTHPAPFU]
      ,[ResultsHPV]
      ,[CaseMgmt]
      ,[LSU]
      ,[CDC]
      ,[oldautonum]
      ,[SSMA_TimeStamp]
  FROM [695Test].[dbo].[PAPResults] 
  --where [ResultsHPV] != 'N' and [ResultsHPV] != 'P' and [ResultsHPV] != 'X' and [ResultsHPV] != 'U' and [ResultsHPV] is not null
  -- where RECPAP != 'N' and RECPAP != 'O' and RECPAP != 'S' and RECPAP != 'R' and RECPAP != 'P' and RECPAP != 'W' and RECPAP != 'L' and RECPAP != 'G' and RECPAP != 'B' and RECPAP != 'T' and RECPAP != 'C' and RECPAP != 'U' and RECPAP != 'V'
  /* I think the PAPRCMD and RECPAP columns are duped attributes but with unique data, though they were used simultaeously with bad input. I think. */
  -- where PAPRCMD != 'N' and PAPRCMD != 'O'--is not null AND RECPAP is not null
  
  -- where ResultsPap != 'N' AND ResultsPap != 'AU' AND ResultsPap != 'R' AND ResultsPap != 'HS' AND ResultsPap != 'E' AND ResultsPap != 'Y' AND ResultsPap != 'SQ' AND ResultsPap != 'LS' AND ResultsPap != 'O' AND ResultsPap != 'AH' AND ResultsPap != 'AG' AND ResultsPap != 'A'
  --where RSPAP IS NOT NULL AND ResultsPap IS NOT NULL
  -- where PapType is not null and PapType != 'C' and PapType != 'L' -- only two are other and unknown