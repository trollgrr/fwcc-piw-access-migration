--if OBJECT_ID(N'dbo.TempTable', N'U') is not null
--drop table TempTable 
--select top 10 * from dbo.OutputTable2
SELECT [MasterPatientIndex].[PtID] as record_id
	  ,RIGHT('9999999999' + CONVERT(NVARCHAR, LTRIM([MRN])), 10) as mrn /*suppresses errors but adds bad data*/
	  ,[FNAME] + ' ' + [LNAME] as "name"
	  , "ssn_provided" =
	  case when datalength([SSNUM]) < 5 then '0'
	  when datalength([SSNUM]) < 18 then 'ssn prov error'
	  else '1'
	  end      
	  ,"hx_diabetes" =
	  case when [ScreeningData].[DIABETES] = 'N' then '0'
	  when [ScreeningData].[DIABETES] = 'Y' then '1'
	  else '9'
	  end
	  ,"ever_ca"=
	  case when [ScreeningData].[YOUHADCA] = 'N' then '0'
	  when [ScreeningData].[YOUHADCA] = 'Y' then '1'
	  else '9'
	  end
	  ,"ca_type___1"=
	  case when [ScreeningData].[BREASTCA] = 'N' then '0'
	  when [ScreeningData].[BREASTCA] = 'Y' then '1'
	  when [ScreeningData].[BREASTCA] = 'L' then '1'
	  when [ScreeningData].[BREASTCA] = 'R' then '1'
	  when [ScreeningData].[BREASTCA] = 'B' then '1'
	  when [ScreeningData].[BREASTCA] = 'U' then '1'
	  else '0'
	  end
	  ,"ca_type___2"=
	  case when [ScreeningData].[PROSTATECA] = 'N' then '0'
	  when [ScreeningData].[PROSTATECA] = 'Y' then '1'
	  when [ScreeningData].[PROSTATECA] = 'D' then '1'
	  else '0'
	  end
	  ,"rel_ca___1" =
	  case when [FamilyCancer].[MEMBER] = 'M' then '1'
	  when [FamilyCancer].[MEMBER] = 'F' then '1'
	  else '0'
	  end
	  INTO OutputTable2
	  FROM [695Test].[dbo].[MasterPatientIndex]
	  INNER JOIN [695Test].[dbo].[ScreeningData] ON [ScreeningData].PtID = [MasterPatientIndex].PtID
	  LEFT JOIN [695Test].[dbo].[FamilyCancer] ON [FamilyCancer].PtID = [MasterPatientIndex].PtID AND [FamilyCancer].MEMNUM = '1'
	  --PIVOT(
	  --sum([FamilyCancer].PtID)
	  --FOR [MEMBER] in ([rel_ca___1],[rel_ca___2],[rel_ca___3])
	  --) as rotated (one, two, three);
	  
	  
	  
	  
	  --select PtID, MEMBER from [FamilyCancer] where [FamilyCancer].MEMNUM = '2'
	  --alter table TempTable add [rel_ca___2] varchar(255)
	  --alter table TempTable add [rel_ca___3] varchar(255)
	  --alter table TempTable add [rel_ca___4] varchar(255)
	  --alter table TempTable add [rel_ca___5] varchar(255)
	  --alter table TempTable add [rel_ca___6] varchar(255)
	  --alter table TempTable add [rel_ca___9] varchar(255)
	  --select rel_ca___2, record_id from TempTable
	  ----insert into TempTable ([rel_ca___2])

	  --update TempTable set TempTable.[rel_ca___1] =
	  --case when [FamilyCancer].[MEMBER] = 'M' then '1'
	  --when [FamilyCancer].[MEMBER] = 'F' then '1'
	  --else '0'
	  --end
	  --from [FamilyCancer] where [FamilyCancer].PtID = [TempTable].record_id

	  --update TempTable set TempTable.[rel_ca___2] =
	  --case when [FamilyCancer].[MEMBER] = 'S' then '1'
	  --when [FamilyCancer].[MEMBER] = 'B' then '1'
	  ----else '0'
	  --end
	  --from [FamilyCancer] where [FamilyCancer].PtID = [TempTable].record_id

	  --update TempTable set TempTable.[rel_ca___3] =
	  --case when [FamilyCancer].[MEMBER] = 'C' then '1'
	  ----else '0'
	  --end
	  --from [FamilyCancer] where [FamilyCancer].PtID = [TempTable].record_id

	  --update TempTable set TempTable.[rel_ca___4] =
	  --case when [FamilyCancer].[MEMBER] = 'MM' then '1'
	  --when [FamilyCancer].[MEMBER] = 'MF' then '1'
	  --when [FamilyCancer].[MEMBER] = 'GM' then '1'
	  --when [FamilyCancer].[MEMBER] = 'GF' then '1'
	  ----else '0'
	  --end
	  --from [FamilyCancer] where [FamilyCancer].PtID = [TempTable].record_id

	  --update TempTable set TempTable.[rel_ca___5] =
	  --case when [FamilyCancer].[MEMBER] = 'MA' then '1'
	  --when [FamilyCancer].[MEMBER] = 'MU' then '1'
	  --when [FamilyCancer].[MEMBER] = 'FA' then '1'
	  --when [FamilyCancer].[MEMBER] = 'FU' then '1'
	  ----else '0'
	  --end
	  --from [FamilyCancer] where [FamilyCancer].PtID = [TempTable].record_id and MEMNUM = 3

	  --update TempTable set TempTable.[rel_ca___6] =
	  --case when [FamilyCancer].[MEMBER] = 'O' then '1'
	  --when [FamilyCancer].[MEMBER] = 'D' then '1'
	  --else '0'
	  --end
	  --from [FamilyCancer] where [FamilyCancer].PtID = [TempTable].record_id

	  --update TempTable set TempTable.[rel_ca___9] =
	  --case when [FamilyCancer].[MEMBER] = 'U' then '1'
	  --when [FamilyCancer].[MEMBER] = 'X' then '1'
	  --else '0'
	  --end
	  --from [FamilyCancer] where [FamilyCancer].PtID = [TempTable].record_id