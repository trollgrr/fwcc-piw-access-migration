/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (10) [FamCaID]
      ,[PtID]
      ,[MEMNUM]
      ,[MEMBER]
      ,[AGEATDX]
      ,[CASITE]
      ,[MEMNAME]
  FROM [695Test].[dbo].[FamilyCancer] where [PtID] = 16032--[MEMBER] is null and [MEMNAME] is not null

/* Discovered data fields that were set in the incorrect column; this will find and fix them all except that it doesnt*/
--update dbo.[FamilyCancer] set [MEMBER] =
--case when [MEMNAME] = 'niece' then 'FN'
--when [MEMNAME] = 'uncle' then 'FU'
--when [MEMNAME] = 'MA6' then 'MA'
--when [MEMNAME] = 'aunt' then 'MA'
--when [MEMNAME] = 'grandpa' then 'MF'
--when [MEMNAME] = 'grandma' then 'MM'
--when [MEMNAME] = 'nephew' then 'MN'
--else 'error'
--end