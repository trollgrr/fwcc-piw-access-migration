/****** Script for SelectTopNRows 245 command from SSMS  ******/
SELECT TOP (1000) [ColonID]
      ,[PtID]
      ,[VisitID]
      ,[VisitDate]
      ,[LASTUPDT]
      ,[ColonTEST]
      ,[CBIOPSY]
      ,[POLYPS]
      ,[TUMOR]
      ,[IBDS]
      ,[DIV]
      ,[OTHERTEST]
      ,[PLYPSZ]
      ,[PLYPLOC]
      ,[PLYPHIS]
      ,[TMORLOC]
      ,[OTHERESULTS]
      ,[COLONCA]
      ,[CPSTAGET]
      ,[CPSTtext]
      ,[CPSTAGEN]
      ,[CPSNtext]
      ,[CPSTAGEM]
      ,[CPSMtext]
      ,[LSU]
      ,[oldautonum]
      ,[SSMA_TimeStamp]
  FROM [695Test].[dbo].[ColonResults]
  -- where [ColonTEST] != 'C' and [ColonTEST] != 'O' and [ColonTEST] != 'B' and [ColonTEST] != 'F' and [ColonTEST] != 'A'
  -- where OTHERESULTS IS NOT NULL -- 177 rows
  -- where OTHERTEST IS NOT NULL --57 rows