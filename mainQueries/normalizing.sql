--select top 10 * from [FamilyCancer]  --[MasterPatientIndex] --order by PtID --[ScreeningData]

--ALTER Table [OutputTable] add [rel_ca___1] int, [rel_ca___2] int, [rel_ca___3] int, [rel_ca___4] int, [rel_ca___5] int, [rel_ca___6] int, [rel_ca___9] int;
--ALTER Table [OutputTable] add [fam_hx_type_ca___1] int, [fam_hx_type_ca___2] int, [fam_hx_type_ca___3] int, [fam_hx_type_ca___4] int, [fam_hx_type_ca___5] int, [fam_hx_type_ca___6] int, [fam_hx_type_ca___9] int;
--select * from [OutputTable2];
--select distinct PtID from [FamilyCancer]
drop table if exists FamCaRankNormalized
drop table if exists OutputTable2
with FamCaRankNormalized as (
select ROW_NUMBER() over (PARTITION by PtID ORDER BY MEMNUM) as rank
,MEMNUM
,PtID
,MEMBER
,CASITE
,MEMNAME from [695Test].dbo.[FamilyCancer] 
)
select PtID
,count(case when [MEMBER] = 'M' or [MEMBER] = 'F' then '1' end) as [rel_ca___1]
,count(case when [MEMBER] = 'B' or [MEMBER] = 'S' then '1' end) as [rel_ca___2]
,count(case when [MEMBER] = 'C' then '1' end) as [rel_ca___3]
,count(case when [MEMBER] = 'MM' or [MEMBER] = 'MF' or [MEMBER] = 'GF' or [MEMBER] = 'GF' then '1' end) as [rel_ca___4]
,count(case when [MEMBER] = 'MA' or [MEMBER] = 'MU' or [MEMBER] = 'FA' or [MEMBER] = 'FU' then '1' end) as [rel_ca___5]
,count(case when [MEMBER] = 'O'  then '1' end) as [rel_ca___6]
,count(case when [MEMBER] = 'D' or [MEMBER] = 'U' or [MEMBER] = 'X' then '1' end) as [rel_ca___9]
,count(case when [CASITE] = 'Br' then '1' end) as fam_hx_type_ca___1
,count(case when [CASITE] = 'Pr' then '1' end) as fam_hx_type_ca___2
,count(case when [CASITE] = 'Cn' then '1' end) as fam_hx_type_ca___3
--,count(case when [CASITE] = 'O' then '1' end) as fam_hx_type_ca___4 -- lung only annotated in comments
,count(case when [CASITE] = 'Ut' then '1' end) as fam_hx_type_ca___5 --also endometrial
,count(case when [CASITE] = 'O' then '1' end) as fam_hx_type_ca___6
,count(case when [CASITE] = 'U' or [CASITE] = 'D' or [CASITE] = 'X' then '1' end) as fam_hx_type_ca___9 -- unknown or don't know or refused to answer
INTO OutputTable2
from FamCaRankNormalized f --where PtID = 16320
group by PtID

