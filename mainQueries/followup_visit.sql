/****************************************************************/
/**************** Follow-up Visit Record ************************/
/****************************************************************/
/*
 * The registration, followup_visit and the screening_visit are defined by the redcap_repeat_instrument field
 * If the tuple has a value for an attribute that doesn't match the tuple type, redcap will throw an error
 * To avoid errors and reduce memory overhead, these are split into different queries
 */
SELECT --[VisitData].[VisitID]
       [VisitData].[PtID] as "record_id"
      ,redcap_repeat_instrument = 'followup_visit'
	  ,redcap_repeat_instance = [VisitData].[VisitID] 
	  ,fu_date = FORMAT([VisitData].[VisitDate], 'yyyy-MM-dd')
	  ,fu_loc= -- Follow-up Visit Location -- Only three values in this set, expanded locations later
	  case when [VisitData].[Unit] = 'C1' then '0' -- 0, PIW Clinic
	  when [VisitData].[Unit] = 'M1' then '1' -- 1, Mobile 1
	  when [VisitData].[Unit] = 'M2' then '2' -- 2, Mobile 2
	  -- 3, Mobile 3
	  -- 4, Feist-Weiller
	  -- 5, University Health
	  -- 6, Other
	  else '9'-- 9, Not Specified (lots of nulls in data)
	  end
	  ,fu_loc_details= -- Follow-up Location Details
	  case when [SiteCodeTable].[SiteCodeDesc] is not null then [SiteCodeDesc]
	  else '' end
	  ,fu_reason= -- Type of Follow-Up --Many rows are marked as two types of follow-up exams--cycle values are weird
	  case when [MamResults].[VisitID] is not null 
		OR [BreastBxResults].[VisitID] is not null 
		OR [BreastUSResults].[VisitID] is not null
		OR [SurgicalConsultResults].[VisitID] is not null
		OR [CBEResults].[VisitID] is not null then '1'        -- 1, Breast f/u
	  when [GynConsultResults].[VisitID] is not null
		OR [CervicalBxResults].[VisitID] is not null then '2' -- 2, GYN f/u
	  when [ColonResults].[VisitID] is not null then '3'      -- 3, Colon f/u
	  when [ProstateBxResults].[VisitID] is not null
		OR [PSAResults].[VisitID] is not null then '4'        -- 4, Prostate f/u
	  when [PelvicResults].VisitID is not null then '5'       -- 5, Other f/u
	  else '' end
	  /*Breast Related: Follow-up Procedures*/
	  ,brst_fu_procedure___1= -- 1, Additional Views
	  case when [MamResults].[MAMRCMD] = 'A' then '1' else '0' end
	  ,brst_fu_procedure___2= -- 2, Ultrasound
	  case when [MamResults].[MAMRCMD] = 'U' then '1' else '0' end -- [BreastUSResults].[VisitID] is not null or
	  ,brst_fu_procedure___3= -- 3, Film Comparison
	  case when [MamResults].[MAMRCMD] = '?' then '1' else '0' end
	  ,brst_fu_procedure___4= -- 4, MRI
	  case when [MamResults].[MAMRCMD] = 'M' then '1' else '0' end
	  ,brst_fu_procedure___5=  -- 5, FNA
	  case when [MamResults].[MAMRCMD] = 'F' then '1' else '0' end
	  ,brst_fu_procedure___6= -- 6, Biopsy
	  case when [MamResults].[MAMRCMD] = 'B' then '1' else '0' end
	  ,brst_fu_procedure___7= -- 7, Surgical Consult
	  case when [MamResults].[MAMRCMD] = 'C' then '1' else '0' end
	  ,brst_fu_procedure___8= -- 8, CBE by Consult
	  case when [MamResults].[MAMRCMD] = 'E' then '1' else '0' end
	  ,add_views_result='' -- Additional Views Results --They didn't start this until later, no add views included
	  -- 0, Assessment incomplete
	  -- 1, Negative
	  -- 2, Benign
	  -- 3, Probably benign
	  -- 4, Suspicious
	  -- 5, Highly suspicious
	  -- 6, Confirmed malignancy
	  -- 9, Results pending
	  ,add_views_rec='' -- Additional Views Follow-up Recommendation
	  -- 0, Follow routine screening
	  -- 1, Additional views
	  -- 2, Film Comparison
	  -- 3, Short-term f/u
	  -- 4, CBE by consult
	  -- 5, Ultrasound
	  -- 6, MRI
	  -- 7, Surgical consult
	  -- 8, FNA
	  -- 9, Biopsy
	  -- 10, Obtain definitive treatment
	  -- 11, Other
	  ,ultrasound_result= -- Ultrasound Results -- They changed from 'SCORN' to BIRAD system, 419 and 940 respectively
	  case when [BreastUSResults].[RSUSNew] is not null then [RSUSNew] -- this line is ideal, the others are just guesses on meaning
	  -- 0, Assessment incomplete
	  when [BreastUSResults].[RSUSNew] is null AND [BreastUSResults].[RSBRSTUS] = 'N' then '1'-- 1, Negative
	  -- 2, Benign
	  -- 3, Probably benign
	  when [BreastUSResults].[RSUSNew] is null AND [BreastUSResults].[RSBRSTUS] = 'S' then '4'-- 4, Suspicious
	  -- 5, Highly suspicious
	  when [BreastUSResults].[RSUSNew] is null AND [BreastUSResults].[RSBRSTUS] = 'C' then '6'-- 6, Confirmed malignancy
	  -- 9, Results pending
	  else '' end
	  ,ultrasound_rec= -- Ultrasound Follow-up Recommendation
	  case when [BreastUSResults].[RECUS] = 'N' then '0' -- 0, Follow routine screening
	  when [BreastUSResults].[RECUS] = 'A' then '1' -- 1, Additional views
	  when [BreastUSResults].[RECUS] = '?' then '2' -- 2, Film Comparison
	  when [BreastUSResults].[RECUS] = 'S' then '3' -- 3, Short-term f/u
	  when [BreastUSResults].[RECUS] = 'E' then '4' -- 4, CBE by consult
	  when [BreastUSResults].[RECUS] = 'U' then '5' -- 5, Ultrasound
	  when [BreastUSResults].[RECUS] = 'M' then '6' -- 6, MRI
	  when [BreastUSResults].[RECUS] = 'C' then '7' -- 7, Surgical consult
	  when [BreastUSResults].[RECUS] = 'F' then '8' -- 8, FNA
	  when [BreastUSResults].[RECUS] = 'B' then '9' -- 9, Biopsy
	  when [BreastUSResults].[RECUS] = 'T' then '10'-- 10, Obtain definitive treatment
	  when [BreastUSResults].[RECUS] = 'R' then '11'-- 11, Other
	  else '' end
	  /* There are no film comparisons or MRI results in source */
	  ,film_comp_result='' -- Film Comparison Results
	  -- 0, Assessment incomplete
	  -- 1, Negative
	  -- 2, Benign
	  -- 3, Probably benign
	  -- 4, Suspicious
	  -- 5, Highly suspicious
	  -- 6, Confirmed malignancy
	  -- 9, Results pending
	  ,film_comp_rec='' -- Film Comparison Follow-up Recommendation
	  -- 0, Follow routine screening
	  -- 1, Additional views
	  -- 2, Film Comparison
	  -- 3, Short-term f/u
	  -- 4, CBE by consult
	  -- 5, Ultrasound
	  -- 6, MRI
	  -- 7, Surgical consult
	  -- 8, FNA
	  -- 9, Biopsy
	  -- 10, Obtain definitive treatment
	  -- 11, Other
	  ,mri_result=''-- MRI Results
	  -- 0, Assessment incomplete  
	  -- 1, Negative  
	  -- 2, Benign  
	  -- 3, Probably benign  
	  -- 4, Suspicious  
	  -- 5, Highly suspicious  
	  -- 6, Confirmed malignancy  
	  -- 9, Results pending
	  ,mri_rec='' -- MRI Follow-up Recommendation
	  -- 0, Follow routine screening  
	  -- 1, Additional views  
	  -- 2, Film Comparison  
	  -- 3, Short-term f/u  
	  -- 4, CBE by consult  
	  -- 5, Ultrasound  
	  -- 6, MRI 
	  -- 7, Surgical consult  
	  -- 8, FNA  
	  -- 9, Biopsy  
	  -- 10, Obtain definitive treatment  
	  -- 11, Other
	  ,fna_result= -- FNA Results
	  case when [FNAResults].[RSFNA] = 'F' then '0'-- 0, No fluid/tissue obtained  
	  when [FNAResults].[RSFNA] = 'N' then '1'-- 1, Not suspicious  
	  when [FNAResults].[RSFNA] = 'S' then '2'-- 2, Suspicious  
	  when [FNAResults].[RSFNA] = 'X' then '3'-- 3, Not done  
	  when [FNAResults].[RSFNA] = 'R' then '4'-- 4, Refused  
	  when [FNAResults].[RSFNA] = 'U' then '5'-- 5, Unknown
	  else '' end
	  ,fna_rec= -- FNA Follow-up Recommendation [FNAResults].[RECFNA]
	  case when [FNAResults].[RECFNA] = 'N' then '0'-- 0, Follow routine screening  
	  when [FNAResults].[RECFNA] = 'A' then '1' -- 1, Additional views  
	  when [FNAResults].[RECFNA] = '?' then '2' -- 2, Film Comparison  
	  when [FNAResults].[RECFNA] = 'S' then '3' -- 3, Short-term f/u  
	  when [FNAResults].[RECFNA] = 'E' then '4' -- 4, CBE by consult  
	  when [FNAResults].[RECFNA] = 'U' then '5' -- 5, Ultrasound  
	  when [FNAResults].[RECFNA] = 'M' then '6' -- 6, MRI  
	  when [FNAResults].[RECFNA] = 'C' then '7' -- 7, Surgical consult  
	  when [FNAResults].[RECFNA] = 'F' then '8' -- 8, FNA  
	  when [FNAResults].[RECFNA] = 'B' then '9' -- 9, Biopsy  
	  when [FNAResults].[RECFNA] = 'T' then '10'-- 10, Obtain definitive treatment  
	  when [FNAResults].[RECFNA] = '?' then '11'-- 11, Other
	  else '' end
	  ,bx_type= -- Biopsy Type
	  case when [BreastBxResults].[BXTYPE] = 'I' then '0'-- 0, Incisional  
	  when [BreastBxResults].[BXTYPE] = 'U' then '1'-- 1, Ultrasound guided  
	  when [BreastBxResults].[BXTYPE] = 'N' then '2'-- 2, Needle loc  
	  when [BreastBxResults].[BXTYPE] = 'E' then '3'-- 3, Excisional  
	  when [BreastBxResults].[BXTYPE] = 'S' then '4'-- 4, Stereotactic  
	  when [BreastBxResults].[BXTYPE] = 'C' then '5'-- 5, Core/true cut  
	  when [BreastBxResults].[BXTYPE] = '?' then '6'-- 6, Savi Scout --source has modified radical mastectomy
	  else '' end
	  ,bx_result= -- Biopsy Results
	  case when [BreastBxResults].[BxResult] = 'N' then '0'-- 0, Normal breast tissue  
	  when [BreastBxResults].[BxResult] = 'O' then '1'-- 1, Other benign changes  
	  when [BreastBxResults].[BxResult] = 'H' then '2'-- 2, Hyperplasia  
	  when [BreastBxResults].[BxResult] = '?' then '3'-- 3, Atypical ductal hyperplasia  
	  when [BreastBxResults].[BxResult] = 'D' then '4'-- 4, DCIS  
	  when [BreastBxResults].[BxResult] = 'L' then '5'-- 5, LCIS  
	  when [BreastBxResults].[BxResult] = 'M' then '6'-- 6, Invasive breast CA  
	  when [BreastBxResults].[BxResult] = 'X' then '7'-- 7, Not done  
	  when [BreastBxResults].[BxResult] = 'R' then '8'-- 8, Refused  
	  when [BreastBxResults].[BxResult] = 'U' then '9'-- 9, Unknown
	  else '' end
	  ,bx_rec= -- Biopsy Follow-up Recommendation
	  case when [BreastBxResults].[BxResult] = 'N' then '0'-- 0, Follow routine screening  
	  when [BreastBxResults].[BxResult] = 'A' then '1' -- 1, Additional views  
	  when [BreastBxResults].[BxResult] = '?' then '2' -- 2, Film Comparison  -- no film comparison in source
	  when [BreastBxResults].[BxResult] = 'S' then '3' -- 3, Short-term f/u  
	  when [BreastBxResults].[BxResult] = 'E' then '4' -- 4, CBE by consult  
	  when [BreastBxResults].[BxResult] = 'U' then '5' -- 5, Ultrasound  
	  when [BreastBxResults].[BxResult] = 'M' then '6' -- 6, MRI  
	  when [BreastBxResults].[BxResult] = 'C' then '7' -- 7, Surgical consult  
	  when [BreastBxResults].[BxResult] = 'F' then '8' -- 8, FNA  
	  when [BreastBxResults].[BxResult] = 'B' then '9' -- 9, Biopsy  
	  when [BreastBxResults].[BxResult] = 'T' then '10'-- 10, Obtain definitive treatment  
	  when [BreastBxResults].[BxResult] = '?' then '11'-- 11, Other --could be M for MRI
	  else '' end
	  ,surg_con_result= -- Surgical Consult Result
	  case when [SurgicalConsultResults].[ConsultResults] = 'N' then '0'-- 0, No intervention - routine f/u  
	  when [SurgicalConsultResults].[ConsultResults] = 'S' then '1'-- 1, Short-term f/u  
	  when [SurgicalConsultResults].[ConsultResults] = 'B' then '2'-- 2, Biopsy/FNA recommended  
	  when [SurgicalConsultResults].[ConsultResults] = '?' then '3'-- 3, Ultrasound recommended  
	  when [SurgicalConsultResults].[ConsultResults] = '?' then '4'-- 4, Surgery or treatment recommended  
	  when [SurgicalConsultResults].[ConsultResults] = 'X' then '5'-- 5, Not done  
	  when [SurgicalConsultResults].[ConsultResults] = 'R' then '6'-- 6, Refused  
	  when [SurgicalConsultResults].[ConsultResults] = 'U' then '7'-- 7, Unknown
	  else '' end
	  ,surg_con_rec= -- Surgical Consult Follow-up Recommendation [RECConsult]
	  case when [SurgicalConsultResults].[RECConsult] = 'N' then '0'-- 0, Follow routine screening  
	  when [SurgicalConsultResults].[RECConsult] = 'A' then '1' -- 1, Additional views  
	  when [SurgicalConsultResults].[RECConsult] = '?' then '2' -- 2, Film Comparison  -- no film comparison in source
	  when [SurgicalConsultResults].[RECConsult] = 'S' then '3' -- 3, Short-term f/u  
	  when [SurgicalConsultResults].[RECConsult] = 'E' then '4' -- 4, CBE by consult  
	  when [SurgicalConsultResults].[RECConsult] = 'U' then '5' -- 5, Ultrasound  
	  when [SurgicalConsultResults].[RECConsult] = 'M' then '6' -- 6, MRI  
	  when [SurgicalConsultResults].[RECConsult] = 'C' then '7' -- 7, Surgical consult  
	  when [SurgicalConsultResults].[RECConsult] = 'F' then '8' -- 8, FNA  
	  when [SurgicalConsultResults].[RECConsult] = 'B' then '9' -- 9, Biopsy  
	  when [SurgicalConsultResults].[RECConsult] = 'T' then '10'-- 10, Obtain definitive treatment  
	  when [SurgicalConsultResults].[RECConsult] = '?' then '11'-- 11, Other --could be M for MRI
	  else '' end
	  ,cbe_con_result= -- CBE by Consult Results -- This is the same data as the screening, but with the followup flag set, not sure the difference
	  case when [CBEResults].[CBEResults] = 'N' then '0' --N Normal                       --Normal
	  when [CBEResults].[CBEResults] = 'MS' then '1'  --MS Discrete palp mass/Susp Ca     --1 Discreet [sic] Palpable Mass
	  when [CBEResults].[CBEResults] = 'MB' then '2'  --MB Discrete palp mass/Benign      --2 Discreet Palpable Mass (dx benign)
	  when [CBEResults].[CBEResults] = 'ND' then '3'  --ND Bloody/serous nipple discharge --3 Serous/Bloody Nipple Discharge
	  when [CBEResults].[CBEResults] = 'B' then '4'   --B Benign Finding                  --4 Benign Finding
	  when [CBEResults].[CBEResults] = 'SD' then '5'  --SD Skin Dimpling/Retraction       --5 Skin Dimpling
	  when [CBEResults].[CBEResults] = '?' then '6'                                       --6 Abscess
	  when [CBEResults].[CBEResults] = 'Z' then ''    --Z Not done/normal CBE<12 m
	  when [CBEResults].[CBEResults] = 'R' then ''    --R Refused
	  when [CBEResults].[CBEResults] = 'NS' then ''   --NS Nipple/areolar scaliness
	  when [CBEResults].[CBEResults] = '?' then '9'                                       --9 Results Pending
	  else '' end
	  ,cbe_con_rec= -- CBE by Consult Follow-up Recommendation
	  case when [CBEResults].[RECCBE] = 'N' then '0'-- Follow Routine Screening
	  when [CBEResults].[RECCBE] = 'A' then '1'-- Additional mammography views
	  when [CBEResults].[RECCBE] = '?' then '2'--Film Comparisons (not present in source lookup) --leave blank, this is new
	  when [CBEResults].[RECCBE] = 'S' then '3'--Short-term Follow-up Mam
	  when [CBEResults].[RECCBE] = 'E' then '4'--CBE by Consult
	  when [CBEResults].[RECCBE] = 'U' then '5'--Ultrasound
	  when [CBEResults].[RECCBE] = 'M' then '6'--MRI
	  when [CBEResults].[RECCBE] = 'C' then '7'--Surgery Consultation
	  when [CBEResults].[RECCBE] = 'F' then '8'--FNA
	  when [CBEResults].[RECCBE] = 'B' then '9'-- Biopsy --disregard OR [CBEResults].[RCMDBBX]= 'Y', old db
	  when [CBEResults].[RECCBE] = 'T' then '10'--Obtain Definitive Treatment
	  when [CBEResults].[RECCBE] = '?' then '11'--Other
	  else '' end
	  /* Gyn-related Follow-up Section */
	  ,gyn_fu_procedure___1=-- 1, Pelvic U/S
	  case when [GynConsultResults].[RECConsult] = 'U' then '1' else '0' end
	  ,gyn_fu_procedure___2=  -- 2, Gyn Consult
	  case when [GynConsultResults].[RECConsult] = 'G' then '1' else '0' end
	  ,gyn_fu_procedure___3=  -- 3, Colposcopy
	  case when [GynConsultResults].[RECConsult] = 'P' then '1' else '0' end
	  ,gyn_fu_procedure___4=  -- 4, Other Biopsy
	  case when [GynConsultResults].[RECConsult] = 'B' then '1' else '0' end
	  ,gyn_fu_procedure___5=''  -- 5, ECC (aka EndoCervical Curettage)
	  ,gyn_fu_procedure___6=''  -- 6, EMB (aka EndoMetrial Biopsy)
	  ,gyn_fu_procedure___7=  -- 7, LEEP (aka Loop Electrosurgical Excision Procedure)
	  case when [GynConsultResults].[RECConsult] = 'L' then '1' else '0' end
	  ,gyn_fu_procedure___8=  -- 8, CKC (aka cone)
	  case when [GynConsultResults].[RECConsult] = 'C' then '1' else '0' end
	  ,gyn_fu_procedure___9=  -- 9, Hysterectomy
	  case when [GynConsultResults].[RECConsult] = 'H' then '1' else '0' end
	  ,pelvic_us_result= -- Pelvic Ultrasound Results
	  case when [PelvicUSResults].[RSCVUS] = 'N' then '0'-- 0, Normal 
	  when [PelvicUSResults].[RSCVUS] = 'B' then '1'-- 1, Abnormal not suspicious 
	  when [PelvicUSResults].[RSCVUS] = 'M' then '2'-- 2, Abnormal suspicious 
	  when [PelvicUSResults].[RSCVUS] = 'X' then '3'-- 3, Not done 
	  when [PelvicUSResults].[RSCVUS] = 'R' then '4'-- 4, Refused 
	  when [PelvicUSResults].[RSCVUS] = '?' then '0'-- 5, Other
	  when [PelvicUSResults].[RSCVUS] = 'Y' then '' -- Y Not Indicated
	  when [PelvicUSResults].[RSCVUS] = 'Z' then '' -- Z Not done/normal PE<12 m
	  else '' end
	  ,pelvic_us_rec= -- Pelvic Ultrasound Follow-up Recommendation 
	  case when [PelvicUSResults].[RECUS] = 'N' then '0'-- 0, Pap in 1 year  
	  when [PelvicUSResults].[RECUS] = '?' then '1'-- 1, Pap in 2 years  
	  when [PelvicUSResults].[RECUS] = '?' then '2'-- 2, Pap in 3 years  
	  when [PelvicUSResults].[RECUS] = 'R' then '3'-- 3, Repeat Pap test immediately  
	  when [PelvicUSResults].[RECUS] = 'V' then '4'-- 4, HPV  
	  when [PelvicUSResults].[RECUS] = 'U' then '5'-- 5, Pelvic Ultrasound  
	  when [PelvicUSResults].[RECUS] = 'P' then '6'-- 6, Colposcopy  
	  when [PelvicUSResults].[RECUS] = '?' then '7'-- 7, EMB  
	  when [PelvicUSResults].[RECUS] = '?' then '8'-- 8, ECC  
	  when [PelvicUSResults].[RECUS] = 'B' then '9'-- 9, Other biopsy  
	  when [PelvicUSResults].[RECUS] = 'S' then '10'-- 10, Short-term Follow-up  
	  when [PelvicUSResults].[RECUS] = 'G' then '11'-- 11, Gyn Consult  
	  when [PelvicUSResults].[RECUS] = 'L' then '12'-- 12, LEEP  
	  when [PelvicUSResults].[RECUS] = 'C' then '13'-- 13, CKC (aka Cold Knife Cone)
	  when [PelvicUSResults].[RECUS] = 'T' then '14'-- 14, Obtain Definitive Treatment  
	  when [PelvicUSResults].[RECUS] = 'H' then '15'-- 15, Hysterectomy
	  when [PelvicUSResults].[RECUS] = 'W' then '' --W Women's Health Clinic
	  else '' end
	  ,gyn_con_result= -- GYN Consult Results
	  case when [GynConsultResults].[ConsultResults] = 'N' then '0'-- 0, No intervention needed 
	  when [GynConsultResults].[ConsultResults] = 'S' then '1'-- 1, Short-term f/u  
	  when [GynConsultResults].[ConsultResults] = '?' then '2'-- 2, Pelvic u/s  
	  when [GynConsultResults].[ConsultResults] = 'B' then '3'--B Biopsy/FNA -- 3, Biopsy  
	  when [GynConsultResults].[ConsultResults] = 'X' then '4'-- 4, Not done  
	  when [GynConsultResults].[ConsultResults] = 'R' then '5'-- 5, Refused  
	  when [GynConsultResults].[ConsultResults] = 'U' then '6'--U Unknown -- 6, Other
	  else '' end
	  ,gyn_con_rec= -- GYN Consult Follow-up Recommendation
	  case when [GynConsultResults].[RECConsult] = 'N' then '0'-- 0, Pap in 1 year  
	  when [GynConsultResults].[RECConsult] = '?' then '1'-- 1, Pap in 2 years  
	  when [GynConsultResults].[RECConsult] = '?' then '2'-- 2, Pap in 3 years  
	  when [GynConsultResults].[RECConsult] = 'R' then '3'-- 3, Repeat Pap test immediately  
	  when [GynConsultResults].[RECConsult] = 'V' then '4'-- 4, HPV  
	  when [GynConsultResults].[RECConsult] = 'U' then '5'-- 5, Pelvic Ultrasound  
	  when [GynConsultResults].[RECConsult] = 'P' then '6'-- 6, Colposcopy  
	  when [GynConsultResults].[RECConsult] = '?' then '7'-- 7, EMB  
	  when [GynConsultResults].[RECConsult] = '?' then '8'-- 8, ECC  
	  when [GynConsultResults].[RECConsult] = 'B' then '9'-- 9, Other biopsy  
	  when [GynConsultResults].[RECConsult] = 'S' then '10'-- 10, Short-term Follow-up  
	  when [GynConsultResults].[RECConsult] = 'G' then '11'-- 11, Gyn Consult  
	  when [GynConsultResults].[RECConsult] = 'L' then '12'-- 12, LEEP  
	  when [GynConsultResults].[RECConsult] = 'C' then '13'-- 13, CKC (aka Cold Knife Cone)
	  when [GynConsultResults].[RECConsult] = 'T' then '14'-- 14, Obtain Definitive Treatment  
	  when [GynConsultResults].[RECConsult] = 'H' then '15'-- 15, Hysterectomy
	  when [GynConsultResults].[RECConsult] = 'W' then '' -- W Women's Health Clinic
	  else '' end
/*D*/ ,colpo_satisfactory='' --Colposcopy Satisfactory?
/*D*/ ,colpo_bx= -- Biopsy done during colposcopy?
	  case when [CervicalBxResults].[BxType] = 'P' then '1' else '0' end
	  ,colpo_result= -- Colposcopy Results
	  case when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[BxResults] = 'N' then '0'-- 0, Normal  
	  when [CervicalBxResults].[BxType] = 'P' AND [BxResults] = 'B' then '1'  -- 1, Benign  
	  when [CervicalBxResults].[BxType] = 'P' AND [BxResults] = 'O' then '2'  -- 2, Other nonmalignant abnormality  
	  when [CervicalBxResults].[BxType] = 'P' AND [BxResults] = 'C1' then '3' -- 3, CIN I  
	  when [CervicalBxResults].[BxType] = 'P' AND [BxResults] = 'C2' then '4' -- 4, CIN II  
	  when [CervicalBxResults].[BxType] = 'P' AND [BxResults] = 'C3' then '5' -- 5, CIN III / CIS  
	  when [CervicalBxResults].[BxType] = 'P' AND [BxResults] = '?' then '6'  -- 6, Squamous Cell Carcinoma  
	  when [CervicalBxResults].[BxType] = 'P' AND [BxResults] = 'AC' then '7' -- 7, Adenocarcinoma  
	  when [CervicalBxResults].[BxType] = 'P' AND [BxResults] = 'M' then '8'  -- 8, Invasive Carcinoma  
	  when [CervicalBxResults].[BxType] = 'P' AND [BxResults] = 'X' then '9'  -- 9, Not done  
	  when [CervicalBxResults].[BxType] = 'P' AND [BxResults] = 'R' then '10' -- 10, Refused  
	  when [CervicalBxResults].[BxType] = 'P' AND [BxResults] = '?' then '11'  -- 11, Other
	  else '' end
	  ,colpo_rec= -- Colposcopy Follow-up Recommendation [CervicalBxResults].[RECBX]
	  case when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[RECBX] = '?' then '0'-- 0, Pap in 1 year  
	  when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[RECBX] = '?' then '1' -- 1, Pap in 2 years  
	  when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[RECBX] = 'N' then '2' -- 2, Pap in 3 years  
	  when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[RECBX] = 'R' then '3' -- 3, Repeat Pap test immediately  
	  when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[RECBX] = 'V' then '4' -- 4, HPV  
	  when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[RECBX] = 'U' then '5' -- 5, Pelvic Ultrasound  
	  when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[RECBX] = 'P' then '6' -- 6, Colposcopy  
	  when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[RECBX] = '?' then '7' -- 7, EMB  
	  when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[RECBX] = '?' then '8' -- 8, ECC  
	  when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[RECBX] = 'B' then '9' -- 9, Other biopsy  
	  when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[RECBX] = 'S' then '10'-- 10, Short-term Follow-up  
	  when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[RECBX] = 'G' then '11'-- 11, Gyn Consult  
	  when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[RECBX] = 'L' then '12'-- 12, LEEP  
	  when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[RECBX] = 'C' then '13'-- 13, CKC (cold knife cone)
	  when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[RECBX] = 'T' then '14'-- 14, Obtain Definitive Treatment  
	  when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[RECBX] = 'H' then '15'-- 15, Hysterectomy
	  when [CervicalBxResults].[BxType] = 'P' AND [CervicalBxResults].[RECBX] = 'W' then ''  -- W Women's Health Clinic
	  else '' end
	  ,other_bx_result= -- Other Biopsy Results
	  case when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[BxResults] = 'N' then '0'-- 0, Normal  
	  when [CervicalBxResults].[BxType] = 'B' AND [BxResults] = 'B' then '1'  -- 1, Benign  
	  when [CervicalBxResults].[BxType] = 'B' AND [BxResults] = 'O' then '2'  -- 2, Other nonmalignant abnormality  
	  when [CervicalBxResults].[BxType] = 'B' AND [BxResults] = 'C1' then '3' -- 3, CIN I  
	  when [CervicalBxResults].[BxType] = 'B' AND [BxResults] = 'C2' then '4' -- 4, CIN II  
	  when [CervicalBxResults].[BxType] = 'B' AND [BxResults] = 'C3' then '5' -- 5, CIN III / CIS  
	  when [CervicalBxResults].[BxType] = 'B' AND [BxResults] = '?' then '6'  -- 6, Squamous Cell Carcinoma  
	  when [CervicalBxResults].[BxType] = 'B' AND [BxResults] = 'AC' then '7' -- 7, Adenocarcinoma  
	  when [CervicalBxResults].[BxType] = 'B' AND [BxResults] = 'M' then '8'  -- 8, Invasive Carcinoma  
	  when [CervicalBxResults].[BxType] = 'B' AND [BxResults] = 'X' then '9'  -- 9, Not done  
	  when [CervicalBxResults].[BxType] = 'B' AND [BxResults] = 'R' then '10' -- 10, Refused  
	  when [CervicalBxResults].[BxType] = 'B' AND [BxResults] = '?' then '11'  -- 11, Other
	  else '' end
	  ,other_bx_rec= -- Other Biopsy Follow-up Recommendation
	  case when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[RECBX] = '?' then '0'-- 0, Pap in 1 year  
	  when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[RECBX] = '?' then '1' -- 1, Pap in 2 years  
	  when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[RECBX] = 'N' then '2' -- 2, Pap in 3 years  
	  when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[RECBX] = 'R' then '3' -- 3, Repeat Pap test immediately  
	  when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[RECBX] = 'V' then '4' -- 4, HPV  
	  when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[RECBX] = 'U' then '5' -- 5, Pelvic Ultrasound  
	  when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[RECBX] = 'P' then '6' -- 6, Colposcopy  
	  when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[RECBX] = '?' then '7' -- 7, EMB  
	  when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[RECBX] = '?' then '8' -- 8, ECC  
	  when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[RECBX] = 'B' then '9' -- 9, Other biopsy  
	  when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[RECBX] = 'S' then '10'-- 10, Short-term Follow-up  
	  when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[RECBX] = 'G' then '11'-- 11, Gyn Consult  
	  when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[RECBX] = 'L' then '12'-- 12, LEEP  
	  when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[RECBX] = 'C' then '13'-- 13, CKC (cold knife cone)
	  when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[RECBX] = 'T' then '14'-- 14, Obtain Definitive Treatment  
	  when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[RECBX] = 'H' then '15'-- 15, Hysterectomy
	  when [CervicalBxResults].[BxType] = 'B' AND [CervicalBxResults].[RECBX] = 'W' then ''  -- W Women's Health Clinic
	  else '' end
	  ,ecc_result= -- Endocervical Curettage Results
	  case when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[BxResults] = 'N' then '0'-- 0, Normal  
	  when [CervicalBxResults].[BxType] = 'E' AND [BxResults] = 'B' then '1'  -- 1, Benign  
	  when [CervicalBxResults].[BxType] = 'E' AND [BxResults] = 'O' then '2'  -- 2, Other nonmalignant abnormality  
	  when [CervicalBxResults].[BxType] = 'E' AND [BxResults] = 'C1' then '3' -- 3, CIN I  
	  when [CervicalBxResults].[BxType] = 'E' AND [BxResults] = 'C2' then '4' -- 4, CIN II  
	  when [CervicalBxResults].[BxType] = 'E' AND [BxResults] = 'C3' then '5' -- 5, CIN III / CIS  
	  when [CervicalBxResults].[BxType] = 'E' AND [BxResults] = '?' then '6'  -- 6, Squamous Cell Carcinoma  
	  when [CervicalBxResults].[BxType] = 'E' AND [BxResults] = 'AC' then '7' -- 7, Adenocarcinoma  
	  when [CervicalBxResults].[BxType] = 'E' AND [BxResults] = 'M' then '8'  -- 8, Invasive Carcinoma  
	  when [CervicalBxResults].[BxType] = 'E' AND [BxResults] = 'X' then '9'  -- 9, Not done  
	  when [CervicalBxResults].[BxType] = 'E' AND [BxResults] = 'R' then '10' -- 10, Refused  
	  when [CervicalBxResults].[BxType] = 'E' AND [BxResults] = '?' then '11'  -- 11, Other
	  else '' end
	  ,ecc_rec= -- Endocervical Curettage Follow-up Recommendation
	  case when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[RECBX] = '?' then '0'-- 0, Pap in 1 year  
	  when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[RECBX] = '?' then '1' -- 1, Pap in 2 years  
	  when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[RECBX] = 'N' then '2' -- 2, Pap in 3 years  
	  when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[RECBX] = 'R' then '3' -- 3, Repeat Pap test immediately  
	  when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[RECBX] = 'V' then '4' -- 4, HPV  
	  when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[RECBX] = 'U' then '5' -- 5, Pelvic Ultrasound  
	  when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[RECBX] = 'P' then '6' -- 6, Colposcopy  
	  when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[RECBX] = '?' then '7' -- 7, EMB  
	  when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[RECBX] = '?' then '8' -- 8, ECC  
	  when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[RECBX] = 'B' then '9' -- 9, Other biopsy  
	  when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[RECBX] = 'S' then '10'-- 10, Short-term Follow-up  
	  when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[RECBX] = 'G' then '11'-- 11, Gyn Consult  
	  when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[RECBX] = 'L' then '12'-- 12, LEEP  
	  when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[RECBX] = 'C' then '13'-- 13, CKC (cold knife cone)
	  when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[RECBX] = 'T' then '14'-- 14, Obtain Definitive Treatment  
	  when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[RECBX] = 'H' then '15'-- 15, Hysterectomy
	  when [CervicalBxResults].[BxType] = 'E' AND [CervicalBxResults].[RECBX] = 'W' then ''  -- W Women's Health Clinic
	  else '' end
	  ,emb_result= -- Endometrial Biopsy Results
	  case when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[BxResults] = 'N' then '0'-- 0, Normal  
	  when [CervicalBxResults].[BxType] = 'R' AND [BxResults] = 'B' then '1'  -- 1, Benign  
	  when [CervicalBxResults].[BxType] = 'R' AND [BxResults] = 'O' then '2'  -- 2, Other nonmalignant abnormality  
	  when [CervicalBxResults].[BxType] = 'R' AND [BxResults] = 'C1' then '3' -- 3, CIN I  
	  when [CervicalBxResults].[BxType] = 'R' AND [BxResults] = 'C2' then '4' -- 4, CIN II  
	  when [CervicalBxResults].[BxType] = 'R' AND [BxResults] = 'C3' then '5' -- 5, CIN III / CIS  
	  when [CervicalBxResults].[BxType] = 'R' AND [BxResults] = '?' then '6'  -- 6, Squamous Cell Carcinoma  
	  when [CervicalBxResults].[BxType] = 'R' AND [BxResults] = 'AC' then '7' -- 7, Adenocarcinoma  
	  when [CervicalBxResults].[BxType] = 'R' AND [BxResults] = 'M' then '8'  -- 8, Invasive Carcinoma  
	  when [CervicalBxResults].[BxType] = 'R' AND [BxResults] = 'X' then '9'  -- 9, Not done  
	  when [CervicalBxResults].[BxType] = 'R' AND [BxResults] = 'R' then '10' -- 10, Refused  
	  when [CervicalBxResults].[BxType] = 'R' AND [BxResults] = '?' then '11'  -- 11, Other
	  else '' end
	  ,emb_rec= -- Endometrial Biopsy Follow-up Recommendation
	  case when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[RECBX] = '?' then '0'-- 0, Pap in 1 year  
	  when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[RECBX] = '?' then '1' -- 1, Pap in 2 years  
	  when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[RECBX] = 'N' then '2' -- 2, Pap in 3 years  
	  when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[RECBX] = 'R' then '3' -- 3, Repeat Pap test immediately  
	  when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[RECBX] = 'V' then '4' -- 4, HPV  
	  when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[RECBX] = 'U' then '5' -- 5, Pelvic Ultrasound  
	  when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[RECBX] = 'P' then '6' -- 6, Colposcopy  
	  when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[RECBX] = '?' then '7' -- 7, EMB  
	  when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[RECBX] = '?' then '8' -- 8, ECC  
	  when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[RECBX] = 'B' then '9' -- 9, Other biopsy  
	  when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[RECBX] = 'S' then '10'-- 10, Short-term Follow-up  
	  when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[RECBX] = 'G' then '11'-- 11, Gyn Consult  
	  when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[RECBX] = 'L' then '12'-- 12, LEEP  
	  when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[RECBX] = 'C' then '13'-- 13, CKC (cold knife cone)
	  when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[RECBX] = 'T' then '14'-- 14, Obtain Definitive Treatment  
	  when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[RECBX] = 'H' then '15'-- 15, Hysterectomy
	  when [CervicalBxResults].[BxType] = 'R' AND [CervicalBxResults].[RECBX] = 'W' then ''  -- W Women's Health Clinic
	  else '' end
	  ,leep_result= -- LEEP Results
	  case when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[BxResults] = 'N' then '0'-- 0, Normal  
	  when [CervicalBxResults].[BxType] = 'L' AND [BxResults] = 'B' then '1'  -- 1, Benign  
	  when [CervicalBxResults].[BxType] = 'L' AND [BxResults] = 'O' then '2'  -- 2, Other nonmalignant abnormality  
	  when [CervicalBxResults].[BxType] = 'L' AND [BxResults] = 'C1' then '3' -- 3, CIN I  
	  when [CervicalBxResults].[BxType] = 'L' AND [BxResults] = 'C2' then '4' -- 4, CIN II  
	  when [CervicalBxResults].[BxType] = 'L' AND [BxResults] = 'C3' then '5' -- 5, CIN III / CIS  
	  when [CervicalBxResults].[BxType] = 'L' AND [BxResults] = '?' then '6'  -- 6, Squamous Cell Carcinoma  
	  when [CervicalBxResults].[BxType] = 'L' AND [BxResults] = 'AC' then '7' -- 7, Adenocarcinoma  
	  when [CervicalBxResults].[BxType] = 'L' AND [BxResults] = 'M' then '8'  -- 8, Invasive Carcinoma  
	  when [CervicalBxResults].[BxType] = 'L' AND [BxResults] = 'X' then '9'  -- 9, Not done  
	  when [CervicalBxResults].[BxType] = 'L' AND [BxResults] = 'R' then '10' -- 10, Refused  
	  when [CervicalBxResults].[BxType] = 'L' AND [BxResults] = '?' then '11'  -- 11, Other
	  else '' end
	  ,leep_rec= -- LEEP Follow-up Recommendation
	  case when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[RECBX] = '?' then '0'-- 0, Pap in 1 year  
	  when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[RECBX] = '?' then '1' -- 1, Pap in 2 years  
	  when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[RECBX] = 'N' then '2' -- 2, Pap in 3 years  
	  when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[RECBX] = 'R' then '3' -- 3, Repeat Pap test immediately  
	  when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[RECBX] = 'V' then '4' -- 4, HPV  
	  when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[RECBX] = 'U' then '5' -- 5, Pelvic Ultrasound  
	  when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[RECBX] = 'P' then '6' -- 6, Colposcopy  
	  when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[RECBX] = '?' then '7' -- 7, EMB  
	  when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[RECBX] = '?' then '8' -- 8, ECC  
	  when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[RECBX] = 'B' then '9' -- 9, Other biopsy  
	  when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[RECBX] = 'S' then '10'-- 10, Short-term Follow-up  
	  when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[RECBX] = 'G' then '11'-- 11, Gyn Consult  
	  when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[RECBX] = 'L' then '12'-- 12, LEEP  
	  when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[RECBX] = 'C' then '13'-- 13, CKC (cold knife cone)
	  when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[RECBX] = 'T' then '14'-- 14, Obtain Definitive Treatment  
	  when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[RECBX] = 'H' then '15'-- 15, Hysterectomy
	  when [CervicalBxResults].[BxType] = 'L' AND [CervicalBxResults].[RECBX] = 'W' then ''  -- W Women's Health Clinic
	  else '' end
	  ,ckc_result= -- Cold Knife Cone Results
	  case when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[BxResults] = 'N' then '0'-- 0, Normal  
	  when [CervicalBxResults].[BxType] = 'C' AND [BxResults] = 'B' then '1'  -- 1, Benign  
	  when [CervicalBxResults].[BxType] = 'C' AND [BxResults] = 'O' then '2'  -- 2, Other nonmalignant abnormality  
	  when [CervicalBxResults].[BxType] = 'C' AND [BxResults] = 'C1' then '3' -- 3, CIN I  
	  when [CervicalBxResults].[BxType] = 'C' AND [BxResults] = 'C2' then '4' -- 4, CIN II  
	  when [CervicalBxResults].[BxType] = 'C' AND [BxResults] = 'C3' then '5' -- 5, CIN III / CIS  
	  when [CervicalBxResults].[BxType] = 'C' AND [BxResults] = '?' then '6'  -- 6, Squamous Cell Carcinoma  
	  when [CervicalBxResults].[BxType] = 'C' AND [BxResults] = 'AC' then '7' -- 7, Adenocarcinoma  
	  when [CervicalBxResults].[BxType] = 'C' AND [BxResults] = 'M' then '8'  -- 8, Invasive Carcinoma  
	  when [CervicalBxResults].[BxType] = 'C' AND [BxResults] = 'X' then '9'  -- 9, Not done  
	  when [CervicalBxResults].[BxType] = 'C' AND [BxResults] = 'R' then '10' -- 10, Refused  
	  when [CervicalBxResults].[BxType] = 'C' AND [BxResults] = '?' then '11'  -- 11, Other
	  else '' end
	  ,ckc_rec= -- Cold Knife Cone Follow-up Recommendation
	  case when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[RECBX] = '?' then '0'-- 0, Pap in 1 year  
	  when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[RECBX] = '?' then '1' -- 1, Pap in 2 years  
	  when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[RECBX] = 'N' then '2' -- 2, Pap in 3 years  
	  when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[RECBX] = 'R' then '3' -- 3, Repeat Pap test immediately  
	  when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[RECBX] = 'V' then '4' -- 4, HPV  
	  when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[RECBX] = 'U' then '5' -- 5, Pelvic Ultrasound  
	  when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[RECBX] = 'P' then '6' -- 6, Colposcopy  
	  when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[RECBX] = '?' then '7' -- 7, EMB  
	  when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[RECBX] = '?' then '8' -- 8, ECC  
	  when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[RECBX] = 'B' then '9' -- 9, Other biopsy  
	  when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[RECBX] = 'S' then '10'-- 10, Short-term Follow-up  
	  when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[RECBX] = 'G' then '11'-- 11, Gyn Consult  
	  when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[RECBX] = 'L' then '12'-- 12, LEEP  
	  when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[RECBX] = 'C' then '13'-- 13, CKC (cold knife cone)
	  when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[RECBX] = 'T' then '14'-- 14, Obtain Definitive Treatment  
	  when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[RECBX] = 'H' then '15'-- 15, Hysterectomy
	  when [CervicalBxResults].[BxType] = 'C' AND [CervicalBxResults].[RECBX] = 'W' then ''  -- W Women's Health Clinic
	  else '' end
	  ,hyst_result= -- Hysterectomy Results
	  case when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[BxResults] = 'N' then '0'-- 0, Normal  
	  when [CervicalBxResults].[BxType] = 'H' AND [BxResults] = 'B' then '1'  -- 1, Benign  
	  when [CervicalBxResults].[BxType] = 'H' AND [BxResults] = 'O' then '2'  -- 2, Other nonmalignant abnormality  
	  when [CervicalBxResults].[BxType] = 'H' AND [BxResults] = 'C1' then '3' -- 3, CIN I  
	  when [CervicalBxResults].[BxType] = 'H' AND [BxResults] = 'C2' then '4' -- 4, CIN II  
	  when [CervicalBxResults].[BxType] = 'H' AND [BxResults] = 'C3' then '5' -- 5, CIN III / CIS  
	  when [CervicalBxResults].[BxType] = 'H' AND [BxResults] = '?' then '6'  -- 6, Squamous Cell Carcinoma  
	  when [CervicalBxResults].[BxType] = 'H' AND [BxResults] = 'AC' then '7' -- 7, Adenocarcinoma  
	  when [CervicalBxResults].[BxType] = 'H' AND [BxResults] = 'M' then '8'  -- 8, Invasive Carcinoma  
	  when [CervicalBxResults].[BxType] = 'H' AND [BxResults] = 'X' then '9'  -- 9, Not done  
	  when [CervicalBxResults].[BxType] = 'H' AND [BxResults] = 'R' then '10' -- 10, Refused  
	  when [CervicalBxResults].[BxType] = 'H' AND [BxResults] = '?' then '11'  -- 11, Other
	  else '' end
	  ,hyst_rec= -- Hysterectomy Follow-up Recommendation
	  case when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[RECBX] = '?' then '0'-- 0, Pap in 1 year  
	  when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[RECBX] = '?' then '1' -- 1, Pap in 2 years  
	  when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[RECBX] = 'N' then '2' -- 2, Pap in 3 years  
	  when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[RECBX] = 'R' then '3' -- 3, Repeat Pap test immediately  
	  when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[RECBX] = 'V' then '4' -- 4, HPV  
	  when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[RECBX] = 'U' then '5' -- 5, Pelvic Ultrasound  
	  when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[RECBX] = 'P' then '6' -- 6, Colposcopy  
	  when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[RECBX] = '?' then '7' -- 7, EMB  
	  when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[RECBX] = '?' then '8' -- 8, ECC  
	  when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[RECBX] = 'B' then '9' -- 9, Other biopsy  
	  when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[RECBX] = 'S' then '10'-- 10, Short-term Follow-up  
	  when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[RECBX] = 'G' then '11'-- 11, Gyn Consult  
	  when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[RECBX] = 'L' then '12'-- 12, LEEP  
	  when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[RECBX] = 'C' then '13'-- 13, CKC (cold knife cone)
	  when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[RECBX] = 'T' then '14'-- 14, Obtain Definitive Treatment  
	  when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[RECBX] = 'H' then '15'-- 15, Hysterectomy
	  when [CervicalBxResults].[BxType] = 'H' AND [CervicalBxResults].[RECBX] = 'W' then ''  -- W Women's Health Clinic
	  else '' end
	  /* Colon-related Follow-up Section */ --[ColonTEST]
	  ,colon_fu_procedure___1=''-- 1, GI Consult
	  ,colon_fu_procedure___2= -- 2, Colonoscopy
	  case when [ColonResults].[ColonTEST] = 'C' then '1' else '0' end
	  ,colon_fu_procedure___3=-- 3, Barium Enema X-ray
	  case when [ColonResults].[ColonTEST] = 'B' then '1' else '0' end
	  ,colon_fu_procedure___4=-- 4, Other Colon Test
	  case when [ColonResults].[ColonTEST] = 'O' then '1'
	  when [ColonResults].[ColonTEST] = 'F' then '1'
	  when [ColonResults].[ColonTEST] = 'P' then '1'
	  when [ColonResults].[ColonTEST] = 'A' then '1'
	  else '0' end
	  ,gi_con_result='' -- GI Consult Results
	  -- 0, No intervention  
	  -- 1, Colonoscopy needed  
	  -- 2, Refused  
	  -- 3, Other
	  ,gi_con_rec='' -- GI Consult Follow-up Recommendation
	  -- 0, Normal routine follow-up  
	  -- 1, Colonoscopy  
	  -- 2, Barium Enema X-ray  
	  -- 3, Other Colon Test  
	  -- 4, Short-term f/u
/*D*/ ,cscope_satisfactory='' -- Colonoscopy satisfactory?
/*D*/ ,cscope_findings='' -- Colonoscopy Findings 
/*D*/ ,cscope_bx= -- Biopsy done during colonoscopy?
       case when [ColonTEST] = 'C' AND [CBIOPSY] = 'Y' then '1' else '0' end
/*D*/ ,cscope_bx_result= -- Colonoscopy Biopsy Results
       case when [ColonTEST] = 'C' AND [CBIOPSY] = 'Y' AND [POLYPS] = 'N'
			then 'Cancer: '+[COLONCA]+ ' Tumor: ' +[TUMOR]+ ' IBDS: ' +[IBDS]+ ' Diverticulosis: ' + [DIV] +' Tumor location: ' +[TMORLOC]+ ' Other test: ' +'"'+[OTHERTEST]+'"'
		when [ColonTEST] = 'C' AND [CBIOPSY] = 'Y' AND [POLYPS] = 'Y'
			then 'Cancer: '+[COLONCA]+ ' Polyps found' + ' Tumor: ' +[TUMOR]+ ' IBDS: ' +[IBDS]+ ' Diverticulosis: ' + [DIV] 
			+' Polyp size: ' +cast([PLYPSZ]as varchar(2))+ ' Polyp location: ' +[PLYPLOC]+ ' Other test: ' +'"'+[OTHERTEST]+'"'
	   else '' end
	  ,cscope_rec='' -- Colonoscopy Follow-up Recommendation
	  -- 0, Repeat colonoscopy in 10 years  
	  -- 1, Repeat colonoscopy in 3-5 years  
	  -- 2, Repeat colonoscopy in 1 year  
	  -- 3, Further testing / Surgery required  
	  -- 4, Other
	  ,barium_result='' -- Barium Enema X-ray Results -- there are 16 results for barium, none have easily workable data, but most have data that can be moved manually
	  -- 0, Normal  
	  -- 1, Benign  
	  -- 2, Abnormal not suspicious  
	  -- 3, Abnormal suspicious  
	  -- 4, Further testing / Surgery needed  
	  -- 5, Other
	  ,barium_rec='' -- Barium Enema X-ray Follow-up Recommendation
	  -- 0, Follow routine screening  
	  -- 1, Short-term follow-up  
	  -- 2, Colonoscopy needed  
	  -- 3, Surgery needed  
	  -- 4, Other
	  ,other_colon_test_result='' -- Other Colon Test Results
	  -- 0, Normal  
	  -- 1, Benign  
	  -- 2, Abnormal not suspicious  
	  -- 3, Abnormal suspicious  
	  -- 4, Other
/*D*/ ,other_colon_test_specified= -- What colon test was done?
       case when [ColonResults].[ColonTEST] = 'F' then 'Flexible Sigmoidoscopy'
	   when [ColonResults].[ColonTEST] = 'P' then 'Proctoscopy'
	   when [ColonResults].[ColonTEST] = 'A' then 'Air Contrast BE'
	   when [ColonResults].[ColonTEST] = 'O' AND [OTHERTEST] is not null then '"'+[OTHERTEST]+'"'
	   else '' end
	  ,other_colon_rec='' -- Other Colon Test Follow-up Recommendation
	  -- 0, Normal routine f/u  
	  -- 1, Short-term f/u  
	  -- 2, Further testing / Surgery needed  
	  -- 3, Other
	  /* Prostate-related Follow-up Section */
	  ,prostate_fu_procedure___1='0' -- 1, Urology Consult
	  ,prostate_fu_procedure___2= -- 2, Prostate Biopsy
	  case when [ProstateBxResults].[VisitID] is not null then '1' else '0' end
	  ,uro_con_result='' -- Urology Consult Results
	  -- 0, Normal exam  
	  -- 1, Benign finding  
	  -- 2, Abnormal not suspicious  
	  -- 3, Abnormal suspicious  
	  -- 4, Other
	  ,uro_con_rec='' -- Urology Consult Follow-up Recommendation
	  -- 0, Normal routine f/u  
	  -- 1, Short-term f/u  
	  -- 2, Biopsy  
	  -- 3, Other
	  ,prostate_bx_result= -- Prostate Biopsy Results -- y/n result in source doesn't line up quite right
	  case when [PROSTCA] = 'N' then '0'-- 0, Normal
	  when [PROSTCA] = '?' then '1'-- 1, Benign  
	  when [PROSTCA] = 'Y' then '2'-- 2, Prostate Cancer
	  when [PROSTCA] = '?' then '3'-- 3, Other
	  else '' end
	  ,gleason_score= -- Gleason Score
	  case when [GLEASONB] is not null then [GLEASONB] else '' end
	  ,prostate_bx_rec='' -- Prostate Biopsy Follow-up Recommendation
	  -- 0, Normal routine f/u  
	  -- 1, Short-term f/u  
	  -- 2, Obtain definitive treatment  
	  -- 3, Other
	  ,other_fu_note='' -- Other Follow-up Procedure Note
	  ,next_fu_sched_date='' -- Next Scheduled Follow-up Date
	  ,fu_visit_note='' -- Follow-up Visit Notes, if needed.
	  
  FROM [695Test].[dbo].[VisitData]
  LEFT JOIN [695Test].[dbo].[CBEResults] ON [VisitData].[VisitID] = [CBEResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[PelvicResults] ON [VisitData].[VisitID] = [PelvicResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[PAPResults] ON [VisitData].[VisitID] = [PAPResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[MamResults] ON [VisitData].[VisitID] = [MamResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[FOBTResults] ON [VisitData].[VisitID] = [FOBTResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[ColonResults] ON [VisitData].[VisitID] = [ColonResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[DREResults] ON [VisitData].[VisitID] = [DREResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[PSAResults] ON [VisitData].[VisitID] = [PSAResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[BreastUSResults] ON [VisitData].[VisitID] = [BreastUSResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[PelvicUSResults] ON [VisitData].[VisitID] = [PelvicUSResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[GynConsultResults] ON [VisitData].[VisitID] = [GynConsultResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[BreastBxResults] ON [VisitData].[VisitID] = [BreastBxResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[SiteCodeTable] ON [VisitData].[SITECODE] = [SiteCodeTable].[SiteCode]
  LEFT JOIN [695Test].[dbo].[ProstateBxResults] ON [VisitData].[VisitID] = [ProstateBxResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[CervicalBxResults] ON [VisitData].[VisitID] = [CervicalBxResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[FNAResults] ON [VisitData].[VisitID] = [FNAResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[SurgicalConsultResults] ON [VisitData].[VisitID] = [SurgicalConsultResults].[VisitID]
  where [VisitData].[VisitType] = 'F' OR [VisitData].[VisitType] = 'X'
