/****************************************************************/
/**************** Screening Visit Record ************************/
/****************************************************************/
/*
 * The registration, followup_visit and the screening_visit are defined by the redcap_repeat_instrument field
 * If the tuple has a value for an attribute that doesn't match the tuple type, redcap will throw an error
 * To avoid errors and reduce memory overhead, these are split into different queries
 */

SELECT  [VisitData].[PtID] as "record_id"
      ,redcap_repeat_instrument = 'screening_visit'-- from [VisitTypeTable]
	  
	  --,redcap_repeat_instrument = ''
	  ,redcap_repeat_instance = [VisitData].[VisitID]
	  ,FORMAT([VisitData].[VisitDate], 'yyyy-MM-dd') as scrn_date
	  ,"scrn_loc" = --theres a sitecode in the table that references exactly where the mobile unit was when the eval was done, but that doesnt seem to fit in the redcap database
	  case when [VisitData].[Unit] = 'C1' then '0'
	  when [VisitData].[Unit] = 'M1' then '1'
	  when [VisitData].[Unit] = 'M2' then '2'
	  else '6'
	  end
	  ,"height"=
	  case when [PhysicalResults].[HEIGHTFT]*12+[PhysicalResults].[HEIGHTIN] > 40 AND [HEIGHTFT]*12+[PhysicalResults].[HEIGHTIN] < 100 then cast([PhysicalResults].[HEIGHTFT]*12+[PhysicalResults].[HEIGHTIN]as varchar(2)) -- if someone is 10 inches tall, they've got bigger problems
	  else '' end -- 1877 is probably that tall, but 1992 goes from 5'9 to zero then to 6'10
	  ,"weight"=
	  case when [PhysicalResults].[WEIGHTLB] > 80 AND [PhysicalResults].[WEIGHTLB] < 1000 then cast([PhysicalResults].[WEIGHTLB] as varchar(3))
	  else '' end
	  ,"systolic"=
	  case when [PhysicalResults].[SYSPRESS] > 40 AND [PhysicalResults].[SYSPRESS] < 1000 then cast([PhysicalResults].[SYSPRESS] as varchar(3)) --record 153 is 2020...delete manually
	  else '' end
	  ,[PhysicalResults].[DIAPRESS] as "diastolic"
	  ,"diastolic"=
	  case when [PhysicalResults].[DIAPRESS] > 40 AND [PhysicalResults].[DIAPRESS] < 1000 then cast([PhysicalResults].[DIAPRESS] as varchar(3))
	  else '' end
	  ,"smoke" =
	  case when [695Test].[dbo].[ScreeningData].[EVERTOB] = 'N' then '0'
	  when [ScreeningData].[NOWTOB] = 'Y' then '1'
	  when [ScreeningData].[NOWTOB] = 'N' then '2'
	  else '9' end
	  /* 1, CBE | 2, Pelvic | 3, PAP | 4, HPV | 5, Scr Mmg | 6, FOBT | 7, FIT | 8, DRE | 9, PSA | 10, DEXA */
	  /********* This is kinda complex, visit reason doesn't line up quite right in the db with existing tables *******/
	  ,"visit_reason___1" = --'0' --CBE clinical breast exam
	  case when [CBEResults].[VisitID] is not null then '1'
	  else '0' end
	  ,"visit_reason___2" = --Pelvic
	  case when [PelvicResults].[PelvicID] is not null then '1'
	  else '0' end
	  ,"visit_reason___3" = --PAP
	  case when [PAPResults].[PAPID] is not null then '1'
	  else '0' end
	  ,"visit_reason___4" = '0' --HPV (grab the result and mark it true)
	  ,"visit_reason___5" = --Scr Mmg most likely mammogram screening
	  case when [MamResults].[MAMID] is not null then '1'
	  else '0' end
	  -- The FOBT table has a FOBTKit and FITKit, but adding those (334 FIT/8791 FOBT doesn't equal 14218 total table contents, so there are FOBTs that aren't kits)
	  ,"visit_reason___6" = --FOBT most likely Colon, fecal occult blood test
	  case when [FOBTResults].[FOBTID] is not null  then '1' --and [FOBTResults].[FOBTID] != 0
	  else '0' end
	  ,"visit_reason___7" = --FIT, fecal immunochemical test
	  case when [FOBTResults].[FITKit] is not null then '1' -- and [FOBTResults].[FITKit] != 0
	  else '0' end
	  ,"visit_reason___8" = --DRE, digital rectal exam 8549
	  case when [DREResults].[DREID] is not null then '1'
	  else '0' end
	  ,"visit_reason___9" = --PSA prostate
	  case when [PSAResults].[PSAID] is not null then '1'
	  else '0' end
	  ,"visit_reason___10" = '0'--dual-energy x-ray absorptiometry
	  ,"cbe_results" =  
	  case when [CBEResults].[CBEResults] = 'N' then '0' --N Normal                       --Normal
	  when [CBEResults].[CBEResults] = 'MS' then '1'  --MS Discrete palp mass/Susp Ca     --1 Discreet [sic] Palpable Mass
	  when [CBEResults].[CBEResults] = 'MB' then '2'  --MB Discrete palp mass/Benign      --2 Discreet Palpable Mass (dx benign)
	  when [CBEResults].[CBEResults] = 'ND' then '3'  --ND Bloody/serous nipple discharge --3 Serous/Bloody Nipple Discharge
	  when [CBEResults].[CBEResults] = 'B' then '4'   --B Benign Finding                  --4 Benign Finding
	  when [CBEResults].[CBEResults] = 'SD' then '5'  --SD Skin Dimpling/Retraction       --5 Skin Dimpling
	  when [CBEResults].[CBEResults] = '?' then '6'                                       --6 Abscess
	  when [CBEResults].[CBEResults] = 'Z' then ''    --Z Not done/normal CBE<12 m
	  when [CBEResults].[CBEResults] = 'R' then ''    --R Refused
	  when [CBEResults].[CBEResults] = 'NS' then ''   --NS Nipple/areolar scaliness
	  when [CBEResults].[CBEResults] = '?' then '9'                                       --9 Results Pending
	  else '' end
	  ,"cbe_rec" = -- [FUBREAST] as source lookup table
	  case when [CBEResults].[RECCBE] = 'N' then '0'-- Follow Routine Screening
	  when [CBEResults].[RECCBE] = 'A' then '1'-- Additional mammography views
	  when [CBEResults].[RECCBE] = '?' then '2'--Film Comparisons (not present in source lookup) --leave blank, this is new
	  when [CBEResults].[RECCBE] = 'S' then '3'--Short-term Follow-up Mam
	  when [CBEResults].[RECCBE] = 'E' then '4'--CBE by Consult
	  when [CBEResults].[RECCBE] = 'U' then '5'--Ultrasound
	  when [CBEResults].[RECCBE] = 'M' then '6'--MRI
	  when [CBEResults].[RECCBE] = 'C' then '7'--Surgery Consultation
	  when [CBEResults].[RECCBE] = 'F' then '8'--FNA
	  when [CBEResults].[RECCBE] = 'B' then '9'-- Biopsy --disregard OR [CBEResults].[RCMDBBX]= 'Y', old db
	  when [CBEResults].[RECCBE] = 'T' then '10'--Obtain Definitive Treatment
	  when [CBEResults].[RECCBE] = '?' then '11'--Other
	  else '' end
	  ,"cbe_comments" = ''
	  /* Found in hidden table in results, just click around where there should be some menu items, it was a bug that never got fixed*/
	  ,"pelvic_results" =
	  case when [PelvicResults] = 'N' then '0' --normal
	  when [PelvicResults].[PelvicResults] = 'B' then '1' --B Abnormal Not Sus Ca --1 Benign Finding (no f/u needed)
	  when [PelvicResults].[PelvicResults] = '?' then '2'                         --2 Benign Finding (patient requests f/u)
	  when [PelvicResults].[PelvicResults] = 'M' then '3' --M Abnormal, susp ca   --3 Abnormal (needs f/u)
	  when [PelvicResults].[PelvicResults] = '?' then '9'                          --9 Results Pending
	  when [PelvicResults].[PelvicResults] = 'Y' then ''  --Y Not indicated
	  when [PelvicResults].[PelvicResults] = 'R' then ''  --R Refused
	  when [PelvicResults].[PelvicResults] = 'X' then ''  --X Not done
	  when [PelvicResults].[PelvicResults] = 'Z' then ''  --Z Not done/normal PE < 12 m
	  else '' end
	  /* from FUPelvic table*/
	  ,"pelvic_rec" =
	  case when [PelvicResults].[RECPE] = '?' then '0' --Pap in 1 year NOSRPWLGBTCUVO
	  when [PelvicResults].[RECPE] = 'W' then '1'  -- W Women's Health Clinic      --Pap in 2 years This does not line up!
	  when [PelvicResults].[RECPE] = 'N' then '2'  -- N Follow routine screening   -- Pap in 3 years --
	  when [PelvicResults].[RECPE] = 'R' then '3'  -- R Repeat Pap test immediately  --
	  when [PelvicResults].[RECPE] = 'V' then '4'  -- V HPV Reflex --
	  when [PelvicResults].[RECPE] = 'U' then '5'  -- U Pelvic Ultrasound --
	  when [PelvicResults].[RECPE] = 'P' then '6'  -- P Colposcopy --
	  when [PelvicResults].[RECPE] = 'O' then '7'  -- 7 EMB (aka endometrial biopsy) --This does not line up! O is not in source table...but it is in the source table o_O
	  when [PelvicResults].[RECPE] = '?' then '8'  -- 8 ECC (aka endocervical curettage)
	  when [PelvicResults].[RECPE] = 'B' then '9'  -- B Other biopsy --
	  when [PelvicResults].[RECPE] = 'S' then '10' -- S Short-term Follow-up --
	  when [PelvicResults].[RECPE] = 'G' then '11' -- G Gyn Consult --
	  when [PelvicResults].[RECPE] = 'L' then '12' -- L LEEP --
	  when [PelvicResults].[RECPE] = 'C' then '10' -- C CKC (aka conization of cervix)
	  when [PelvicResults].[RECPE] = 'T' then '14' -- T Obtain Definitive Treatment--
	  when [PelvicResults].[RECPE] = 'H' then '15' -- H Hysterectomy--
	  else '' end
	  ,"pelvic_notes" = ''
	  /* again no legend yet*/
	  ,pap_results =
	  case when [PAPResults].[ResultsPap] = 'N' then '0' -- Negative (no details)
	  when [PAPResults].[ResultsPap] = 'E' then '1'      -- Negative (additional details) --O
	  when [PAPResults].[ResultsPap] = 'A' then '2'      -- ASCUS --Y
	  when [PAPResults].[ResultsPap] = 'LS' then '3'     -- LSIL
	  when [PAPResults].[ResultsPap] = 'HS' then '4'     -- HSIL
	  when [PAPResults].[ResultsPap] = 'AH' then '5'     -- ASC-H --SQ
	  when [PAPResults].[ResultsPap] = 'AG' then '6'     -- AGUS --AU
	  when [PAPResults].[ResultsPap] = 'R' then '9'      -- Results Pending
	  else '' end
	  ,"pap_rec" =
	  case when [PAPResults].[RECPAP] = '?' then '0' --0 Pap in 1 year OO
	  when [PAPResults].[RECPAP] = '?' then '1' --1 Pap in 2 years
	  when [PAPResults].[RECPAP] = 'N' then '2' --N Follow Routine Screening    --2 Pap in 3 years
	  when [PAPResults].[RECPAP] = 'R' then '3' --R Repeat Pap immed            --3 Repeat Pap test immediately
	  when [PAPResults].[RECPAP] = 'V' then '4' --V HPV Reflex                  --4 HPV
	  when [PAPResults].[RECPAP] = 'U' then '5' --U Pelvic Ultrasound           --5 Pelvic Ultrasound
	  when [PAPResults].[RECPAP] = 'P' then '6' --P Colposcopy                  --6 Colposcopy
	  when [PAPResults].[RECPAP] = '?' then '7' --7 EMB
	  when [PAPResults].[RECPAP] = '?' then '8' --8 ECC
	  when [PAPResults].[RECPAP] = 'B' then '9' --B Other Biopsy                --9 Other biopsy
	  when [PAPResults].[RECPAP] = 'S' then '10'--S Short-term Follow-up        --10 Short-term Follow-up
	  when [PAPResults].[RECPAP] = 'G' then '11'--G Gyn Consult                 --11 Gyn Consult
	  when [PAPResults].[RECPAP] = 'L' then '12'--L LEEP                        --12 LEEP
	  when [PAPResults].[RECPAP] = 'C' then '13'--C Cone                        --13 CKC
	  when [PAPResults].[RECPAP] = 'T' then '14'--T Obtain Definitive Treatment --14 Obtain Definitive Treatment
	  when [PAPResults].[RECPAP] = 'H' then '15'--H Hysterectomy                --15 Hysterectomy
	  when [PAPResults].[RECPAP] = 'W' then ''  --W Women's Health Clinic
	  when [PAPResults].[RECPAP] = 'O' then ''  --O Oh no there's no legend entry
	  else '' end
	  ,"pap_details" =
	  case when [PAPResults].[OTHPAPRS] is not null then '"'+[PAPResults].[OTHPAPRS]+'"'
	  else '' end
	  ,"hpv_results" =
	  case when [PAPResults].[ResultsHPV] = 'N' then '0' --0 Negative
	  when [PAPResults].[ResultsPap] = 'P' then '1' --1 Positive
	  when [PAPResults].[ResultsPap] = 'X' then ''  --X Not Done --values in table with no destination
	  when [PAPResults].[ResultsPap] = 'U' then ''  --U Unknown  --values in table with no destination
	  when [PAPResults].[ResultsPap] = '?' then '9' --9 Results Pending
	  else '' end
	  ,"hpv_rec" = '' -- I can't find anything for this, probably doesn't exist
	  ,"scrn_mammo_type" = -- d and s are screening and diagnostic, data don't exist in access
	  case when [MamResults].[TypeMam] = '?' then '1'--1 Conventional Digital 
	  when [MamResults].[TypeMam] = '?' then '2' --2 Tomosynthesis
	  when [MamResults].[TypeMam] = 'D' then ''  --D Diagnostic --if D, then result should be in the add views, bilateral or unilateral
	  when [MamResults].[TypeMam] = 'S' then ''  --S Screening
	  when [MamResults].[TypeMam] IS NULL AND [MamResults].[MAMID] IS NOT NULL then '9' --9 Unknown
	  else '' end
	  ,"mammo_results" = --These results line up with birad numbers, an industry standard assessment system
	  case when [MamResults].[RSMAM] = '0' then '0'  -- Assessment Incomplete
	  when [MamResults].[RSMAM] = '1' then '1'       -- Negative
	  when [MamResults].[RSMAM] = '2' then '2'       -- Benign
	  when [MamResults].[RSMAM] = '3' then '3'       -- Probably Benign
	  when [MamResults].[RSMAM] = '4' then '4'       -- Suspicious
	  when [MamResults].[RSMAM] = '5' then '5'       -- Highly Suspicious
	  when [MamResults].[RSMAM] = '6' then '6'       -- Confirmed Malignant
	  when [MamResults].[RSMAM] = '?' then '9'       -- Results Pending
	  else '' end
	  ,"mammo_rec" =
	  case when [MamResults].[MAMRCMD] = 'N' then '0' -- Follow Routine Screening
	  when [MamResults].[MAMRCMD] = 'A' then '1' -- Additional mammography views
	  when [MamResults].[MAMRCMD] = '?' then '2' -- Film Comparisons --film comps didn't exist in old system
	  when [MamResults].[MAMRCMD] = 'S' then '3' -- Short-term Follow-up
	  when [MamResults].[MAMRCMD] = 'E' then '4' -- CBE by Consult
	  when [MamResults].[MAMRCMD] = 'U' then '5' -- Ultrasound
	  when [MamResults].[MAMRCMD] = 'M' then '6' -- MRI
	  when [MamResults].[MAMRCMD] = 'C' then '7' -- Surgical Consultation
	  when [MamResults].[MAMRCMD] = 'F' then '8' -- FNA
	  when [MamResults].[MAMRCMD] = 'B' then '9' -- Biopsy
	  when [MamResults].[MAMRCMD] = 'T' then '10' -- Obtain Definitive Treatment
	  when [MamResults].[MAMRCMD] = '?' then '11' -- Other
	  else '' end
	  ,"mammo_note" = -- this field doesn't technically match, but has a lot of comments that look valuable but jumbled -- pitch it
	  case when [MamResults].[OTHMAMFU] is not null then '"'+[MamResults].[OTHMAMFU]+'"'
	  else '' end
	  ,"fobt_results" =  -- FOBT (Fecal Occult Blood Test) Results
	  case when [FOBTResults].[FOBTKit] is not null then [FOBTResults].[FOBTKit]
	  else '' end
	  ,"fit_results" =  -- FIT (Fecal Immunochemical Test) Results
	  case when [FOBTResults].[FITKit] is not null then [FOBTResults].[FITKit]
	  else '' end
	  ,"colo_rec" =
	  case when [ColonResults].[ColonTEST] = 'A' then '0'-- 0, Annual FOBT/FIT Follow-up Needed
	  when [ColonResults].[ColonTEST] = 'C' then '0'-- 1, Colonoscopy Needed
	  when [ColonResults].[ColonTEST] = 'O' then '0'-- 9, Pending
	  when [ColonResults].[ColonTEST] = 'B' then '0'
	  when [ColonResults].[ColonTEST] = 'F' then '0'
	  else '' end
	  ,"colo_note" =
	  case when [ColonResults].[OTHERESULTS] IS NOT NULL AND [ColonResults].[OTHERTEST] IS NOT NULL
		then CONCAT('Other colon results: ', [ColonResults].[OTHERESULTS], 'Other test: ', [OTHERTEST])
	  when [ColonResults].[OTHERESULTS] IS NOT NULL AND [ColonResults].[OTHERTEST] IS NULL
		then CONCAT('Other colon results: ', [ColonResults].[OTHERESULTS])
	  when [ColonResults].[OTHERESULTS] IS NULL AND [ColonResults].[OTHERTEST] IS NOT NULL
	  	then CONCAT('Other test: ', [ColonResults].[OTHERTEST])
	  else ''
	  end
	  ,"dre_results" = -- Digital Rectal Exam Results
	  case when [DREResults].[RSDRE] = 'N' then '0'-- 0, Normal
	  when [DREResults].[RSDRE] = 'B' then '1'-- 1, Abnormal, No Follow-up Needed
	  when [DREResults].[RSDRE] = 'O' then '2'-- 2, Abnormal, Needs Follow-up
	  when [DREResults].[RSDRE] = 'X' then '9'-- 9, Results Pending
	  when [DREResults].[RSDRE] = 'S' then '0'
	  when [DREResults].[RSDRE] = 'R' then '0'
	  when [DREResults].[RSDRE] = 'A' then '0'
	  when [DREResults].[RSDRE] = 'U' then '0'
	  else '' end
	  ,"psa_value" =  -- PSA (Prostate Specific Antigen) Value
	  case when [PSAResults].[RSPSA] = -9 then '' -- where RSPSA = -9 -- 20 rows
	  when [PSAResults].[RSPSA] is not null then [PSAResults].[RSPSA]
	  else '' -- where RSPSA > 100 -- 10 rows, one is over 2000
	  end --9327 rows total
	  ,"psa_high" = '' -- PSA Out of Normal Range (age and race specific)
	  -- 0, No
	  -- 1, Yes
	  -- The boolean was not recorded in the old system, but possible to write code that would create it
	  ,"prost_rec" = -- Prostate Screening Follow-up Recommendations
	  case when [PSAResults].[RECPSA] = 'N' then '0' -- 0, Annual Follow-up
	  when [PSAResults].[RECPSA] = 'U' then '1' -- 1, Needs Urology Consult
	  when [PSAResults].[RECPSA] = 'O' then '9' -- 9, Unknown
	  else '' end
	  ,"prostate_note" = '' -- Prostate Exam Note
	  ,"dexa_results" = '' -- DEXA Bone Scan Results
	  -- 0, Normal
	  -- 1, Abnormal
	  -- 9, Results Pending
	  ,"dexa_rec" = '' -- DEXA Scan Follow-up Recommendations
	  -- 0, No Follow-up Needed
	  -- 1, Consult Referring Physician
	  -- 9, Unknown
	  ,"dexa_note" = '' -- DEXA_NOTES
	  ,"blood_donor" = '' -- Did patient donate a  research blood specimen for the Biorepository
	  -- 0, No
	  -- 1, Yes
	  ,"comment" = '' -- Add Comments about this Screening Visit if Needed
	  ,"sched_scrn_date" = '' -- Next Screening Visit Date -- this would take a lot of work to add, but not sure that it has value to customer
	  ,"sched_fu_date" = '' -- Scheduled Follow-up Visit Date
	  ,"fu_status" = '' -- Follow-up status
	  -- 0, Complete
	  -- 1, Incomplete
	  -- 2, Non-Compliant
	  -- 3, Refused
	  -- 4, Lost-to-FU
	  -- 9, Unknown
	  ,"fu_comments" = '' -- Patient Discussion/Notification Notes
	  
  FROM [695Test].[dbo].[VisitData]
  INNER JOIN [695Test].[dbo].[PhysicalResults] ON [VisitData].[VisitID] = [PhysicalResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[SiteCodeTable] ON [VisitData].[SITECODE] = [SiteCodeTable].[SiteCode] --26k results with no site id
  INNER JOIN [695Test].[dbo].[ScreeningData] ON [ScreeningData].[PtID] = [VisitData].[PtID] --rethink this, is each visit going to have its own screening data? i think so
  LEFT JOIN [695Test].[dbo].[CBEResults] ON [VisitData].[VisitID] = [CBEResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[PelvicResults] ON [VisitData].[VisitID] = [PelvicResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[PAPResults] ON [VisitData].[VisitID] = [PAPResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[MamResults] ON [VisitData].[VisitID] = [MamResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[FOBTResults] ON [VisitData].[VisitID] = [FOBTResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[ColonResults] ON [VisitData].[VisitID] = [ColonResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[DREResults] ON [VisitData].[VisitID] = [DREResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[PSAResults] ON [VisitData].[VisitID] = [PSAResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[BreastUSResults] ON [VisitData].[VisitID] = [BreastUSResults].[VisitID]
  LEFT JOIN [695Test].[dbo].[PelvicUSResults] ON [VisitData].[VisitID] = [PelvicUSResults].[VisitID]
  where [VisitData].[VisitType] = 'A' OR [VisitData].[VisitType] = 'S' --screening or additional screening
  order by record_id