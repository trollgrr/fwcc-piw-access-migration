/* Caution!!!! This will destroy data!!!*/
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (10) [PtID]
      ,[SITECODE],[Unit],[DATESCRN],[RCNTSCRN],[LASTUPDT]
      ,[MRN],[LNAME],[FNAME],[MAIDEN],[SEX],[DOB]
	  ,[FNAME] + ' ' + [LNAME] as nombre
	  , "SSN_Provided" =
	  case when datalength([SSNUM])=0 then 'No'
	  when datalength([SSNUM]) < 5 then 'error'
	  else 'Yes'
	  end
      ,[STREET],[CITY],[PARISH],[STATE],[ZIPCODE]
      ,[PHONE1],[PHONE2],[EMPHONE],[EMCONTACT]
	  ,[SSNUM],[MARITAL],[RACE],[RACE2],[RACE3],[ETHNICITY],[INCOME],[STATUS],[DEATH],[UCOD],[CDCELIGIBLE],[oldautonum],[MobileID]
  FROM [695Test].[dbo].[MasterPatientIndex] /*where [RACE] = 'E' */

  UPDATE [695Test].[dbo].[MasterPatientIndex] SET [STREET] = cast(abs(checksum(NewId()) % 1001) as varchar) + ' '
  + substring('BCDFGTLNLMNPQRSTRSTL', (abs(checksum(newid())) % 20)+1, 1)
  + substring('AEIOU', (abs(checksum(newid())) % 5)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTUVWXY Z', (abs(checksum(newid())) % 27)+1, 1)
  + substring('AEIOU', (abs(checksum(newid())) % 5)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTUVWXY Z', (abs(checksum(newid())) % 27)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTUVWXY Z', (abs(checksum(newid())) % 27)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTUVWXY ''Z', (abs(checksum(newid())) % 28)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTU!%1&-Z', (abs(checksum(newid())) % 27)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTUVWXY Z', (abs(checksum(newid())) % 27)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTUVWXY Z', (abs(checksum(newid())) % 27)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTUVWXY Z', (abs(checksum(newid())) % 27)+1, 1)
  + ' Apt '
  + substring('ABCDEFGHIJKLMNOPQ123456789', (abs(checksum(newid())) % 26)+1, 1)
  UPDATE [695Test].[dbo].[MasterPatientIndex] SET [PHONE1] = substring('456789', (abs(checksum(newid())) % 6)+1, 1)
  + substring('10', (abs(checksum(newid())) % 2)+1, 1)
  + substring('456789', (abs(checksum(newid())) % 6)+1, 1)
  + substring('56789', (abs(checksum(newid())) % 5)+1, 1)
  + substring('1234567890', (abs(checksum(newid())) % 10)+1, 1)
  + substring('1234567890', (abs(checksum(newid())) % 10)+1, 1)
  + substring('1234567890', (abs(checksum(newid())) % 10)+1, 1)
  + substring('1234567890', (abs(checksum(newid())) % 10)+1, 1)
  + substring('1234567890', (abs(checksum(newid())) % 10)+1, 1)
  + substring('1234567890', (abs(checksum(newid())) % 10)+1, 1)
  UPDATE [695Test].[dbo].[MasterPatientIndex] SET [PHONE2] = convert(varchar,abs(checksum(NewId()) % 100)+400)+convert(varchar,abs(checksum(NewId()) % 100)+200)+convert(varchar,abs(checksum(NewId()) % 10000))
  UPDATE [695Test].[dbo].[MasterPatientIndex] SET [EMPHONE] = convert(varchar,abs(checksum(NewId()) % 100)+400)+convert(varchar,abs(checksum(NewId()) % 100)+200)+convert(varchar,abs(checksum(NewId()) % 10000))
  UPDATE [695Test].[dbo].[MasterPatientIndex] SET [SSNUM] = substring('23456789', (abs(checksum(newid())) % 8)+1, 1)
    + substring('1234567890', (abs(checksum(newid())) % 10)+1, 1)
  + substring('1234567890', (abs(checksum(newid())) % 10)+1, 1)
  + substring('1234567890', (abs(checksum(newid())) % 10)+1, 1)
  + substring('1234567890', (abs(checksum(newid())) % 10)+1, 1)
  + substring('1234567890', (abs(checksum(newid())) % 10)+1, 1)
  + substring('1234567890', (abs(checksum(newid())) % 10)+1, 1)
  + substring('1234567890', (abs(checksum(newid())) % 10)+1, 1)
  + substring('123456789', (abs(checksum(newid())) % 9)+1, 1)
  UPDATE [695Test].[dbo].[MasterPatientIndex] SET [EMCONTACT] = substring('BCDFGTLNLMNPQRSTRSTL', (abs(checksum(newid())) % 20)+1, 1)
  + substring('AEIOU', (abs(checksum(newid())) % 5)+1, 1)
  + substring('BCDFGTLNLMNPQRSTRSTL', (abs(checksum(newid())) % 20)+1, 1)
  + substring('AEIOU', (abs(checksum(newid())) % 5)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTUVWXY Z', (abs(checksum(newid())) % 27)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTUVWXY Z', (abs(checksum(newid())) % 27)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTUVWXY ''Z', (abs(checksum(newid())) % 28)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTU!%1&-Z', (abs(checksum(newid())) % 27)+1, 1)
  + substring('BCDFGTLNLMNPQRSTRSTL', (abs(checksum(newid())) % 20)+1, 1)
  + substring('AEIOU', (abs(checksum(newid())) % 5)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTUVWXY Z', (abs(checksum(newid())) % 27)+1, 1)
  UPDATE [695Test].[dbo].[MasterPatientIndex] SET [LNAME] 
  = substring('BCDFGTLNLMNPQRSTRSTL', (abs(checksum(newid())) % 20)+1, 1)
  + substring('AEIOU', (abs(checksum(newid())) % 5)+1, 1)
  + substring('BCDFGTLNLMNPQRSTRSTL', (abs(checksum(newid())) % 20)+1, 1)
  + substring('AEIOU', (abs(checksum(newid())) % 5)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTUVWXY ''Z', (abs(checksum(newid())) % 28)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTU!%1&-Z', (abs(checksum(newid())) % 27)+1, 1)
  + substring('BCDFGTLNLMNPQRSTRSTL', (abs(checksum(newid())) % 20)+1, 1)
  + substring('AEIOU', (abs(checksum(newid())) % 5)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTUVWXY Z', (abs(checksum(newid())) % 27)+1, 1)
  UPDATE [695Test].[dbo].[MasterPatientIndex] SET [FNAME] 
  = substring('BCDFGTLNLMNPQRSTRSTL', (abs(checksum(newid())) % 20)+1, 1)
  + substring('AEIOU', (abs(checksum(newid())) % 5)+1, 1)
  + substring('BCDFGTLNLMNPQRSTRSTL', (abs(checksum(newid())) % 20)+1, 1)
  + substring('AEIOU', (abs(checksum(newid())) % 5)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTUVWXY ''Z', (abs(checksum(newid())) % 28)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTU!%1&-Z', (abs(checksum(newid())) % 27)+1, 1)
  + substring('BCDFGTLNLMNPQRSTRSTL', (abs(checksum(newid())) % 20)+1, 1)
  + substring('AEIOU', (abs(checksum(newid())) % 5)+1, 1)
  + substring('ABCDEFGHIJKLMNOPQRSTUVWXY Z', (abs(checksum(newid())) % 27)+1, 1)
  UPDATE [695Test].[dbo].[MasterPatientIndex] SET [MRN] = str(abs(checksum(newId())))

  /* Fixing bad specific values that were auto-generated above  */
  UPDATE [695Test].[dbo].[MasterPatientIndex] SET [SSNUM] = abs(checksum(NewId()) % 1000000001) where [PtID] = 4
  UPDATE [695Test].[dbo].[MasterPatientIndex] SET [PHONE2] = convert(varchar,abs(checksum(NewId()) % 100)+400)+convert(varchar,abs(checksum(NewId()) % 100)+200)+convert(varchar,abs(checksum(NewId()) % 10000)) where [PtID] = 10