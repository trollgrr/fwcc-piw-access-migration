
/***********Clear old tables on debug run*************/
drop table if exists FamCaRankNormalized;
drop table if exists OutputTable2;
/**********Get rid of some tricky data ****************/
UPDATE [695Test].dbo.[MasterPatientIndex] SET [ZIPCODE] = '0'
where [PtID] = '6485';
UPDATE [695Test].dbo.[MasterPatientIndex] SET [ZIPCODE] = '0'
where [PtID] = '5464';
/**********Generate denormalization tables in lieu of pivot*****/

with FamCaRankNormalized as (
select ROW_NUMBER() over (PARTITION by PtID ORDER BY MEMNUM) as rank
,MEMNUM
,PtID
,MEMBER
,CASITE
,MEMNAME from [695Test].dbo.[FamilyCancer] 
)
select PtID
,count(case when [MEMBER] = 'M' or [MEMBER] = 'F' then '1' end) as [rel_ca___1]
,count(case when [MEMBER] = 'B' or [MEMBER] = 'S' then '1' end) as [rel_ca___2]
,count(case when [MEMBER] = 'C' then '1' end) as [rel_ca___3]
,count(case when [MEMBER] = 'MM' or [MEMBER] = 'MF' or [MEMBER] = 'GF' or [MEMBER] = 'GF' then '1' end) as [rel_ca___4]
,count(case when [MEMBER] = 'MA' or [MEMBER] = 'MU' or [MEMBER] = 'FA' or [MEMBER] = 'FU' then '1' end) as [rel_ca___5]
,count(case when [MEMBER] = 'O'  or [MEMBER] = 'MN' then '1' end) as [rel_ca___6]
,count(case when [MEMBER] = 'D' or [MEMBER] = 'U' or [MEMBER] = 'X' then '1' end) as [rel_ca___9]
,count(case when [CASITE] = 'Br' then '1' end) as fam_hx_type_ca___1
,count(case when [CASITE] = 'Pr' then '1' end) as fam_hx_type_ca___2
,count(case when [CASITE] = 'Cn' then '1' end) as fam_hx_type_ca___3
,count(case when [MEMNAME] = 'lung' then '1' end) as fam_hx_type_ca___4 -- lung only annotated in comments
,count(case when [CASITE] = 'Ut' then '1' end) as fam_hx_type_ca___5 --also endometrial
,count(case when [CASITE] = 'O' then '1' end) as fam_hx_type_ca___6
,count(case when [CASITE] = 'U' or [CASITE] = 'D' or [CASITE] = 'X' then '1' end) as fam_hx_type_ca___9 -- unknown or don't know or refused to answer
INTO OutputTable2
from FamCaRankNormalized f --where PtID = 16320
group by PtID

/**********Do the pt query with denormalized data**************/
SELECT [MasterPatientIndex].[PtID] as record_id
	  ,"regis_date"=
	  case when [DATESCRN] is not null then FORMAT([DATESCRN], 'yyyy-MM-dd') else '' end
	  ,[FNAME] + ' ' + [LNAME] as "name"
      ,RIGHT('9999999999' + CONVERT(NVARCHAR, LTRIM([MasterPatientIndex].[MRN])), 10) as mrn /*suppresses errors but adds bad data, should just use the number straight*/ --fix the padding to the left
	  , "ssn_provided" =
	  case when datalength([SSNUM]) < 18 then '0' else '1' end--drop all zeroes or nines
	  ,SUBSTRING([SSNUM],1,3) + '-' + SUBSTRING([SSNUM],4,2) + '-' + SUBSTRING([SSNUM],6,4)as ssn
      ,[STREET] as "address"
	  ,"city" =
	  case when [CITY] is not null then '"'+[CITY]+'"' else '' end
	  ,"parish" = -- lots of missing data in this attribute that I've tried to compensate for
	  case when CAST([PARISH] AS int) is not null and CAST([PARISH] AS int) > 0 then CAST([PARISH] AS int)
	  when [CITY] = 'Shreveport' OR [CITY] = 'Sherveport' OR [CITY] = 'Keithville' OR [CITY] = 'Greenwood' OR [CITY] = 'Blanchard'
	  OR [CITY] = 'Vivian' OR [CITY] = 'Mooringsport' then '9'                                                    -- Caddo
	  when [CITY] = 'Bossier City' OR [CITY] = 'Benton' OR [CITY] = 'Haughton' OR [CITY] = 'Bossier' 
	  OR [CITY] = 'Plain Dealing' OR [CITY] = 'Elm Grove' OR [CITY] = 'Princeton' then '8'                        --Bossier
	  when [CITY] = 'Mansfield' OR [CITY] = 'Logansport' OR [CITY] = 'Stonewall' OR [CITY] = 'Frierson' 
	  OR [CITY] = 'Keatchie' OR [CITY] = 'Grand Cane' OR [CITY] = 'Pelican' then '16'                             -- DeSoto
	  when [CITY] = 'Homer' OR [CITY] = 'Haynesville' then '14'                                                   -- Claiborne
	  when [CITY] = 'Coushatta' then '41'                                                                         -- Red River
	  when [CITY] = 'Sibley' OR [CITY] = 'Shongaloo' OR [CITY] = 'Hefflin' OR [CITY] = 'Minden' OR [CITY] = 'Heflin' 
	  OR [CITY] = 'Doyline' OR [CITY] = 'Springhill' OR [CITY] = 'Dubberly' OR [CITY] = 'Cotton Valley' then '60' -- Webster
	  when [CITY] = 'Zwolle' OR [CITY] = 'Florien' OR [CITY] = 'Pleasant Hill' OR [CITY] = 'Many' then '43'       -- Sabine
	  when [CITY] = 'Tallulah' then '33'                                                  -- Madison
	  when [CITY] = 'Dubach' OR [CITY] = 'Ruston' then '31'                               -- Lincoln
	  when [CITY] = 'Mangham' then '42'                                                   -- Richland
	  when [CITY] = 'Rosepine' OR [CITY] = 'Anacoco' then '58'                            -- Vernon
	  when [CITY] = 'Deridder' then '6'                                                   -- Beauregard
	  when [CITY] = 'Jonesboro' then '25'                                                 -- Jackson
	  when [CITY] = 'Campti' OR [CITY] = 'Natchez' OR [CITY] = 'Natchitoches' OR [CITY] = 'Robeline' then '35'   -- Natchitoches 
	  when [CITY] = 'Winnsboro' then '21'                                                 -- Franklin
	  when [CITY] = 'Castor' then '7'                                                     -- Bienville
	  when [CITY] = 'Winnfield' OR [CITY] = 'Dodson' then '64'                            -- Winn
	  else  '99' end
	  ,"state"=
	  case when [STATE] is not null then [STATE]
	  else '' end
	  ,"zipcode"= 
	  case when [ZIPCODE] is not null  AND REPLACE(REPLACE([ZIPCODE], '.', ''), '\', '') > 11111 AND [ZIPCODE] != ' 7110' then LEFT(REPLACE(REPLACE([ZIPCODE], '.', ''), '\', ''), 5)
	  else '' end
      ,RTRIM(RIGHT('0000000000' + CONVERT(NVARCHAR, LTRIM([PHONE1])), 10)) as home_phone
	  ,RTRIM(RIGHT('0000000000' + CONVERT(NVARCHAR, LTRIM([PHONE2])), 10)) as cell_phone
	  ,'' as other_phone
	  ,'' as email
	  ,"other_contact" =
	  case when datalength(LTRIM([EMCONTACT]))=0 then '0' else '1' end
	  ,[EMCONTACT] as other_name
	  ,RIGHT('0000000000' + CONVERT(NVARCHAR, LTRIM([EMPHONE])), 10) as contact_phone
	  ,'' as other_related
	  ,'' as num_household
	  ,"income" = --note: this is an arbitrary decision on data
	  case when [INCOME] = '<10' then '5000'
	  when [INCOME] = '<15' then '10000'
	  when [INCOME] = '<20' then '15000'
	  when [INCOME] = '<25' then '20000'
	  when [INCOME] = '<40' then '35000'
	  when [INCOME] = '40+' then '40000'
	  else '1000'
	  end
	  ,"payer___0" = -- 0, Self Pay
	  case when [ScreeningData].[NOINS] = 'Y' then '1'
	  else '0'
	  end
	  ,"payer___1" = -- 1, Free Care
	  case when [ScreeningData].[NOINS] = 'N' then '1'
	  else '0'
	  end
	  ,"payer___2" = -- 2, Grant
	  case when [ScreeningData].[NOINS] = 'NULL' then '1' /* this is a hack to show some data but the data are ambiguous, since there are added columns in new dataset*/
	  else '0'
	  end
	  ,"payer___3" = -- 3, Medicaid
	  case when [ScreeningData].[MEDICAID] = 'Y' then '1'
	  else '0'
	  end
	  ,"payer___4" = -- 4, Medicare
	  case when [ScreeningData].[MEDICARE] = 'Y' then '1'
	  else '0'
	  end
	  ,"payer___5" = -- 5, Private Insurance
	  case when [ScreeningData].[PRIVATE] = 'Y' then '1'
	  else '0'
	  end
	  ,"pvt_ins_carrier" = -- Name of Private Insurance Carrier
	  case when [ScreeningData].[PRIVATE] = 'Y' then '"'+[ScreeningData].OTHINS+'"'
	  else '' end
	  ,'' as "outreachpartners___1" -- Outreach Partners -- not an option, so drop it like it is
	  -- 1, Former LBCHP
	  -- 2, Komen
	  -- 3, David Raines
	  -- 4, Amerihealth
	  -- 5, Rapides
	  -- 6, MLK
	  -- 7, Nursing Homes
	  -- 8, Mail-outs
	  ,"sex" =
	  case when [SEX] = 'F' then '0'-- 0, Female
	  when [SEX] = 'M' then '1' -- 1, Male
	  else '' -- Need to kill 8459
	  end
	  ,"dob"=
	  case when [DOB] is not null then FORMAT([DOB], 'yyyy-MM-dd') else '' end
	  ,"married" = -- Marital Status
	  case when [MARITAL] = 'N' then '0' -- 0, Single
	  when [MARITAL] = 'M' then '1' -- 1, Married
	  when [MARITAL] = 'P' then '2' -- 2, Life Partner
	  when [MARITAL] = 'D' then '3' -- 3, Divorced
	  when [MARITAL] = 'W' then '4' -- 4, Widowed
	  when [MARITAL] = 'U' then '9' -- 9, Unknown
	  else '9' 
	  -- 5, Other
	  end
	  ,"hispanic" =
	  case when [ETHNICITY] = 'H' then '1' -- 1, Yes 
	  when [ETHNICITY] = 'U' then '2' -- 2, Not Specified
	  else '0'-- 0, No
	  end
	  ,"race___0" = -- Race (multiple answers possible)
	  case when [RACE] = 'W' then '1' -- 0, White
	  when [RACE2] = 'W' then '1'
	  when [RACE3] = 'W' then '1'
	  else '0'
	  end
	  ,"race___1" =
	  case when [RACE] = 'B' then '1' -- 1, Black
	  when [RACE2] = 'B' then '1'
	  when [RACE3] = 'B' then '1'
	  else '0'
	  end
	  ,"race___2" =
	  case when [RACE] = 'A' then '1' -- 2, Asian
	  when [RACE2] = 'A' then '1'
	  when [RACE3] = 'A' then '1'
	  else '0'
	  end
	  ,"race___3" =
	  case when [RACE] = 'N' then '1' -- 3, Native American
	  when [RACE2] = 'N' then '1'
	  when [RACE3] = 'N' then '1'
	  else '0'
	  end
	  ,"race___4" =
	  case when [RACE] = 'P' then '1' -- 4, Pacific Islander
	  when [RACE2] = 'P' then '1'
	  when [RACE3] = 'P' then '1'
	  else '0'
	  end
	  ,"race___9" =
	  case when [RACE] = 'U' then '1' -- 9, Not Specified
	  when [RACE2] = 'U' then '1'
	  when [RACE3] = 'U' then '1'
	  else '0'
	  end
	  ,'' as hx_hypertension -- History of high blood pressure? -- not asked
	  -- 0, No
	  -- 1, Yes
	  -- 9, Unknown
	  ,"hx_diabetes" = -- History of Diabetes?
	  case when [ScreeningData].[DIABETES] = 'N' then '0'-- 0, No
	  when [ScreeningData].[DIABETES] = 'Y' then '1'-- 1, Yes
	  else '9'-- 9, Unknown
	  end
	  ,"ever_ca"= -- Have you ever had cancer?
	  case when [ScreeningData].[YOUHADCA] = 'N' then '0'-- 0, No
	  when [ScreeningData].[YOUHADCA] = 'Y' then '1'-- 1, Yes
	  else '9'-- 9, Unknown
	  end
	  ,"ca_type___1"= -- Type of Cancer (multiple possible)
	  case when [ScreeningData].[BREASTCA] = 'N' then '0' -- 1, Breast
	  when [ScreeningData].[BREASTCA] = 'Y' then '1'
	  when [ScreeningData].[BREASTCA] = 'L' then '1'
	  when [ScreeningData].[BREASTCA] = 'R' then '1'
	  when [ScreeningData].[BREASTCA] = 'B' then '1'
	  when [ScreeningData].[BREASTCA] = 'U' then '1'
	  else '0'
	  end
	  ,"ca_type___2"= -- 2, Prostate
	  case when [ScreeningData].[PROSTATECA] = 'N' then '0'
	  when [ScreeningData].[PROSTATECA] = 'Y' then '1'
	  when [ScreeningData].[PROSTATECA] = 'D' then '1'
	  else '0'
	  end
	  ,"ca_type___3"= -- 3, Colon
	  case when [ScreeningData].[COLONCA] = 'N' then '0'
	  when [ScreeningData].[COLONCA] = 'Y' then '1'
	  else '0'
	  end
	  ,"ca_type___4"= -- 4, Lung
	  case when [ScreeningData].[LUNGCA] = 'N' then '0'
	  when [ScreeningData].[LUNGCA] = 'Y' then '1'
	  else '0'
	  end
	  ,"ca_type___5"= -- 5, Endometrial, aka uterine
	  case when [ScreeningData].[UTERINECA] = 'N' then '0'
	  when [ScreeningData].[UTERINECA] = 'Y' then '1'
	  else '0'
	  end
	  ,"ca_type___6"= -- 6, Other
	  case when [ScreeningData].[CERVICALCA] = 'N' AND [OVARIANCA] = 'N' AND [OTHERCA] = 'N' then '0'
	  when [ScreeningData].[CERVICALCA] = 'Y' OR [OVARIANCA] = 'Y' OR [OTHERCA] = 'Y' then '1'
	  when [ScreeningData].[OTHERCA] = 'Y' then '1' 
	  else '0'
	  end
	  ,"ca_type___9"= -- 9, Unknown
	  case when [ScreeningData].[YOUHADCA] = 'D'
	  OR [BREASTCA] = 'D'
	  OR [PROSTATECA] = 'D'
	  OR [COLONCA] = 'D'
	  OR [CERVICALCA] = 'D'
	  OR [UTERINECA] = 'D'
	  OR [OVARIANCA] = 'D'
	  OR [LUNGCA] = 'D'
	  OR [OTHERCA] = 'D' --PtID 5795 has [OTHERCA] = 'D' but [OTHERCATYPE] = 'lung'
	  then '1'
	  else '0'
	  end
	  ,"fam_hx_ca" = -- Family History of Cancer
	  case when [ScreeningData].[FAMHADCA] = 'N' then '0'-- 0, No
	  when [ScreeningData].[FAMHADCA] = 'Y' then '1' -- 1, Yes
	  when [ScreeningData].[FAMHADCA] = 'D' then '9' -- 9, Unknown
	  else '9'
	  end
	  -- 9, Unknown
	  ,"vital_status"= --0, Alive | 1, Not known dead | 2, Dead
	  case when [DEATH] is null then '0' else '2' end
	  ,"dod" =
	  case when [DEATH] is null then ''
	  else FORMAT([DEATH], 'yyyy-MM-dd') end
	  ,'' as cod
	  ,'0' as alerts
	  ,'' as alert_text
	  ,"rel_ca___1" =  -- Relatives With Cancer (multiple possible)
	  case when [rel_ca___1] > 0 then '1'
	  else '0' end
	  ,"rel_ca___2" = 
	  case when [rel_ca___2] > 0 then '1'
	  else '0' end
	  ,"rel_ca___3" = 
	  case when [rel_ca___3] > 0 then '1'
	  else '0' end
	  ,"rel_ca___4" = 
	  case when [rel_ca___4] > 0 then '1'
	  else '0' end
	  ,"rel_ca___5" = 
	  case when [rel_ca___5] > 0 then '1'
	  else '0' end
	  ,"rel_ca___6" = 
	  case when [rel_ca___6] > 0 then '1'
	  else '0' end
	  ,"rel_ca___9" = 
	  case when [rel_ca___9] > 0 then '1'
	  else '0' end
	  ,"fam_hx_type_ca___1" = -- Type of Cancer in Relatives (multiple possible) -- check comments for greats and halfs(let half go as brother)
	  case when [fam_hx_type_ca___1] > 0 then '1'
	  else '0' end
	  ,"fam_hx_type_ca___2" = 
	  case when [fam_hx_type_ca___2] > 0 then '1'
	  else '0' end
	  ,"fam_hx_type_ca___3" = 
	  case when [fam_hx_type_ca___3] > 0 then '1'
	  else '0' end
	  ,"fam_hx_type_ca___4" = 
	  case when [fam_hx_type_ca___4] > 0 then '1' -- found lung in comments
	  else '0' end
	  ,"fam_hx_type_ca___5" = 
	  case when [fam_hx_type_ca___5] > 0 then '1'
	  else '0' end
	  ,"fam_hx_type_ca___6" = 
	  case when [fam_hx_type_ca___6] > 0 then '1'
	  else '0' end
	  ,"fam_hx_type_ca___9" = 
	  case when [fam_hx_type_ca___9] > 0 then '1'
	  else '0' end
  FROM [695Test].[dbo].[MasterPatientIndex]
  INNER JOIN [695Test].[dbo].[ScreeningData] ON [ScreeningData].PtID = [MasterPatientIndex].PtID
  left JOIN [OutputTable2] ON [OutputTable2].PtID = [MasterPatientIndex].PtID
  order by record_id